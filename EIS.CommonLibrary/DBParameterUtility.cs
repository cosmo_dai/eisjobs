﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;

namespace EIS.CommonLibrary
{
    /// <summary>
    /// 欄位更新方式
    /// </summary>
    public enum ColumnIncrementType
    {
        /// <summary>
        /// 欄位值增加一個數值
        /// </summary>
        NumberIncrement,

        /// <summary>
        /// 欄位值左邊追加字符串
        /// </summary>
        StringConcatLeft,

        /// <summary>
        /// 欄位值右邊追加字符串
        /// </summary>
        StringConcatRight,
    }

    #region DBParameterUtility 綁定變量參數處理

    /// <summary>
    /// 綁定變量參數處理工具
    /// </summary>
    public static class DBParameterUtility
    {
        private static bool IsExistParameter(ref List<IDbDataParameter> parameters, string key)
        {

            for (int i = 0; i < parameters.Count; i++)
            {
                if (parameters[i].ParameterName.Equals(key))
                    return true;
            }
            return false;
        }

        private static void addParameter( ref List<IDbDataParameter> parameters, ref string key, string field, object value,DataBaseType dbtype)
        {
            key = field.Replace('.', '_');

            if (IsExistParameter(ref parameters, key))
            {
                for (int i = 1; ; i++)
                {
                    if (!IsExistParameter(ref parameters, key + i.ToString()))
                    {
                        key = key + i.ToString(); break;
                    }
                }
            }
            if (value == null || value.ToString() == string.Empty || value is DateTime && (DateTime)value == DateTime.MinValue)
            {
                parameters.Add(CreateCommandParameter(key, DBNull.Value, dbtype));
            }
            //dbc.AddCommandParameter(parameters, key, DBNull.Value, dbtype);
            else
            {
                parameters.Add(CreateCommandParameter(key, value, dbtype));
                //dbc.AddCommandParameter(parameters, key, value, dbtype);
            }
            switch (dbtype)
            {
                case DataBaseType.Oracle: key = ":" + key; break;
                case DataBaseType.SqlServer: key = "@" + key; break;
                default: key = ":" + key; break;
            }
        }

        public static IDbDataParameter CreateCommandParameter(string parameterName, object parameterValue,DataBaseType dbtype)
        {
            switch (dbtype)
            {
                case DataBaseType.Oracle:
                    return new OracleParameter(parameterName, parameterValue);
                case DataBaseType.SqlServer:
                    return new SqlParameter(parameterName, parameterValue);
                default:
                    throw new Exception(string.Format("Database Provider {0} is not support.", ""));
            }
        }

        /// <summary>
        /// ("COLUMN","Value")             ==> "AND COLUMN = :COLUMN"  (:COLUMN = "Value")
        /// ("COLUMN%","Value")             ==> "AND COLUMN LIKE :COLUMN"  (:COLUMN = "Value%")
        /// ("COLUMN%%","Value")             ==> "AND COLUMN LIKE :COLUMN"  (:COLUMN = "%Value%")
        /// ("%COLUMN","Value")             ==> "AND COLUMN LIKE :COLUMN"  (:COLUMN = "%Value")
        /// ("%COLUMN%","Value")             ==> "AND COLUMN LIKE :COLUMN"  (:COLUMN = "%Value%")
        /// ("COLUMN",""),("COLUMN",null)   ==> 被忽略
        /// ("COLUMN>","Value") ==> "AND COLUMN > :COLUMN"  (:COLUMN = "Value")
        /// ("COLUMN>","") ==> 被忽略
        /// ("COLUMN=?","") ==> "AND COLUMN IS NULL"
        /// ("COLUMN>?",null) ==> "AND COLUMN IS NULL"
        /// ("COLUMN",new object[]{"Value1","Value2"}) ==> "AND COLUMN IN (:COLUMN,:COLUMN1)"  (:COLUMN = "Value",:COLUMN1 = "Value2")
        /// 字段值中如果包含%符號,則在字段名稱未指定比較運算符時使用 LIKE 運算符,如:
        /// ("COLUMN","Value%") ==> "AND COLUMN LIKE :COLUMN"  (:COLUMN = "Value%")
        /// ("COLUMN >","Value%") ==> "AND COLUMN > :COLUMN"  (:COLUMN = "Value%") 
        /// 支持的運算符為 等號,大於號,小於號,不等號
        /// 字段名稱中如果后麵包含?號, 則表示字段值為空時, 使用 IS NULL 運算符,否則忽略該條件
        /// 字段值為DateTime型別也可以, 目前僅支持DateTime和String, 如果傳入的不是DateTime型別,將被自動轉為String
        /// </summary>
        /// <param name="parameters">ref參數 parameters, 返回綁定變量</param>
        /// <param name="args">Where條件參數,格式為: DB字段名稱,字段值,DB字段名稱,字段值,....</param>
        /// <returns>返回不帶Where字樣的Where子句(如: (CODE = :CODE AND NAME = :NAME) ),同時填充參數 paramters</returns>
        public static string GenerateWhereAndParameters(ref List<IDbDataParameter> parameters, DataBaseType dbtype, params object[] args)
        {
            StringBuilder sqlStatementWhere = new StringBuilder();

            if (args != null)
            {
                string field = string.Empty, key = string.Empty, keys = string.Empty;
                for (int i = 0; i < args.Length - 1; i += 2)
                {
                    if (args[i + 1] == null || args[i + 1].ToString().Trim() == string.Empty)
                    {
                        if (args[i].ToString().IndexOf('?') > 0)
                        {
                            sqlStatementWhere.AppendFormat("AND {0} IS NULL ", args[i].ToString().TrimEnd(' ', '=', '>', '<', '?', '!'));
                        }
                    }
                    // 如果值參數是一個數組, 則表示要用 IN 運算符
                    else if (args[i + 1].GetType().IsArray)
                    {
                        Object[] obj = (Object[])args[i + 1];

                        bool useOrExtension = false;
                        foreach (object o in obj)
                        {
                            if (o != null || o.ToString().Trim().Contains("%"))
                            {
                                useOrExtension = true;
                                break;
                            }
                        }

                        bool existNullParameter = false;
                        foreach (object o in obj)
                        {
                            if (o == null || o.ToString().Trim() == string.Empty)
                            {
                                // 如果存在空值,當允許 NULL(即有?) 時, 追加一個 IS NULL 的條件
                                if (args[i].ToString().IndexOf('?') > 0) existNullParameter = true;
                                break;
                            }
                        }

                        keys = string.Empty;
                        field = args[i].ToString().TrimEnd('=', '>', '<', '?', '!'); // 忽略比較運算符,用IN運算符

                        if (useOrExtension == false)
                        {
                            foreach (object o in obj)
                            {
                                if (o != null && o.ToString().Trim() != string.Empty)
                                {
                                    addParameter( ref parameters, ref key, field, o,dbtype);
                                    keys = keys + "," + key;
                                }
                            }
                            if (keys != string.Empty && existNullParameter)
                                sqlStatementWhere.AppendFormat("AND ({0} IS NULL OR {0} IN ({1}) ) ", field, keys.Substring(1));
                            else if (keys != string.Empty)
                                sqlStatementWhere.AppendFormat("AND {0} IN ({1}) ", field, keys.Substring(1));
                            else if (existNullParameter)
                                sqlStatementWhere.AppendFormat("AND {0} IS NULL ", field);
                        }
                        else
                        {
                            StringBuilder tmpSql = new StringBuilder();
                            foreach (object o in obj)
                            {
                                if (o != null && o.ToString().Trim() != string.Empty)
                                {
                                    addParameter(ref parameters, ref key, field, o, dbtype);
                                    if (o.ToString().Contains("%"))
                                        tmpSql.AppendFormat(" OR {0} LIKE {1}", field, key);
                                    else
                                        tmpSql.AppendFormat(" OR {0} = {1}", field, key);
                                }
                            }
                            if (existNullParameter)
                                tmpSql.AppendFormat(" OR {0} IS NULL", field);
                            if (tmpSql.Length > 0)
                                sqlStatementWhere.AppendFormat(" AND ({0})", tmpSql.ToString().Substring(3));
                        }

                    }
                    //如果字段名稱中包含比較運算符
                    else if (args[i].ToString().IndexOfAny(new char[] { '=', '>', '<', '!' }) > 0)
                    {
                        if (args[i + 1] == null || args[i + 1].ToString().Trim() == string.Empty)
                        {
                            if (args[i].ToString().IndexOf('?') > 0)
                            {
                                sqlStatementWhere.AppendFormat("AND {0} IS NULL ", args[i].ToString().TrimEnd(' ', '=', '>', '<', '?', '!'));
                            }
                        }
                        else if (args[i + 1].ToString().Trim() != string.Empty)
                        {
                            field = args[i].ToString().Replace("?", string.Empty);
                            addParameter(ref parameters, ref key, field.TrimEnd(' ', '=', '>', '<', '?', '!'), args[i + 1], dbtype);
                            sqlStatementWhere.AppendFormat("AND {0} {1} ", field, key);
                        }
                    }
                    //如果字段名稱中不包含比較運算符
                    else
                    {
                        if (args[i + 1] == null || args[i + 1].ToString().Trim() == string.Empty)
                        {
                            if (args[i].ToString().IndexOf('?') > 0)
                            {
                                sqlStatementWhere.AppendFormat("AND {0} IS NULL ", args[i].ToString().TrimEnd(' '));
                            }
                        }
                        else if (args[i + 1].ToString().Trim() != string.Empty)
                        {
                            bool useLike = false;
                            field = args[i].ToString().Replace("?", string.Empty);
                            if (field.Contains("%%"))
                            {
                                useLike = true;
                                addParameter(ref parameters, ref key, field.TrimEnd(' ', '%'), "%" + args[i + 1] + "%", dbtype);
                            }
                            else if (field.IndexOf('%') == 0)
                            {
                                useLike = true;
                                if (field.LastIndexOf('%') > 0)
                                    addParameter(ref parameters, ref key, field.Substring(1).TrimEnd(' ', '%'), "%" + args[i + 1] + "%", dbtype);
                                else
                                    addParameter(ref parameters, ref key, field.Substring(1).TrimEnd(' ', '%'), "%" + args[i + 1], dbtype);
                            }
                            else if (field.IndexOf('%') > 0)
                            {
                                useLike = true;
                                addParameter(ref parameters, ref key, field.TrimEnd(' ', '%'), args[i + 1] + "%", dbtype);
                            }
                            else
                            {
                                addParameter(ref parameters, ref key, field.TrimEnd(' '), args[i + 1], dbtype);
                            }
                            if (useLike || args[i + 1].ToString().IndexOf('%') >= 0)
                                sqlStatementWhere.AppendFormat("AND {0} LIKE {1} ", field.Replace("%", string.Empty), key);
                            else
                                sqlStatementWhere.AppendFormat("AND {0} = {1} ", field, key);
                        }
                    }
                }
                if (sqlStatementWhere.ToString() != string.Empty)
                {
                    sqlStatementWhere.Remove(0, 4).Insert(0, " (").Append(") ");
                }
            }

            return sqlStatementWhere.ToString();
        }

        /// <summary>
        /// 根據傳入的參數生成Update SQL語句的Set部份及填充相應綁定變量
        /// </summary>
        /// <param name="parameters">ref參數 parameters, 返回綁定變量</param>
        /// <param name="args">Where條件參數,格式為: DB字段名稱,字段舊值,字段新值,DB字段名稱,字段舊值,字段新值,....</param>
        /// <returns>返回不帶Set的Update Set子句(如: CODE = :CODE,NAME = :NAME),同時填充參數 paramters</returns>
        public static string GenerateUpdateSetsAndParameters(ref List<IDbDataParameter> parameters, DataBaseType dbtype, params object[] args)
        {
            StringBuilder sqlUpdateSets = new StringBuilder();

            if (args != null)
            {
                for (int i = 0; i < args.Length - 1; i += 3)
                {
                    // 欄位舊值和新值相同, 則調過.
                    if (!(((args[i + 1] == null || args[i + 1].ToString() == string.Empty) && (args[i + 2] != null && args[i + 2].ToString() != string.Empty))
                        ||
                    ((args[i + 1] != null && args[i + 1].ToString() != string.Empty) && (args[i + 2] == null || args[i + 2].ToString() == string.Empty))
                        ||
                    (args[i + 1] != null && args[i + 2] != null && args[i + 1].ToString() != args[i + 2].ToString())))
                        continue;

                    string field = args[i].ToString(), key = string.Empty;
                    addParameter(ref parameters, ref key, field, args[i + 2],dbtype);
                    if (args[i + 1] != null && args[i + 1] is ColumnIncrementType)
                    {
                        switch ((ColumnIncrementType)args[i + 1])
                        {
                            case ColumnIncrementType.NumberIncrement:
                                sqlUpdateSets.AppendFormat(", {0} = {0} + {1} ", field, key);
                                break;
                            case ColumnIncrementType.StringConcatLeft:
                                switch (dbtype)
                                {
                                    case DataBaseType.Oracle: sqlUpdateSets.AppendFormat(", {0} = {1} || {0} ", field, key); break;
                                    case DataBaseType.SqlServer: sqlUpdateSets.AppendFormat(", {0} = {1} + {0} ", field, key); break;
                                }
                                break;
                            case ColumnIncrementType.StringConcatRight:
                                switch (dbtype)
                                {
                                    case DataBaseType.Oracle: sqlUpdateSets.AppendFormat(", {0} = {0} || {1} ", field, key); break;
                                    case DataBaseType.SqlServer: sqlUpdateSets.AppendFormat(", {0} = {0} + {1} ", field, key); break;
                                }
                                break;
                        }
                    }
                    else
                    {
                        sqlUpdateSets.AppendFormat(", {0} = {1} ", field, key);
                    }
                }
                if (sqlUpdateSets.ToString() != string.Empty)
                {
                    sqlUpdateSets.Remove(0, 1);
                }
            }

            return sqlUpdateSets.ToString();
        }
    }

    #endregion
}
