﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EIS.CommonLibrary
{
    /// <summary>
    /// 執行資料庫動作
    /// </summary>
    public enum DBAction
    {
        /// <summary>
        /// 新增
        /// </summary>
        Insert,
        /// <summary>
        /// 修改
        /// </summary>
        Update,
        /// <summary>
        /// 刪除
        /// </summary>
        Delete,
        /// <summary>
        /// 查詢
        /// </summary>
        Select,
    }
    /// <summary>
    /// SQL 字串類別
    /// </summary>
    public enum DataBaseType
    {
        /// <summary>
        /// 符合 Oracle 資料庫可執行的 SQL 字串
        /// </summary>
        Oracle,
        /// <summary>
        /// 符合 SqlServer 資料庫可執行的 SQL 字串
        /// </summary>
        SqlServer
    }

    /// <summary>
    /// SQL 欄位運算式
    /// </summary>
    public enum SQLOperator
    {
        /// <summary>
        /// 默認
        /// </summary>
        Default,
        /// <summary>
        /// 等於
        /// </summary>
        Equal,
        /// <summary>
        /// 大於
        /// </summary>
        Bigger,
        /// <summary>
        /// 小於
        /// </summary>
        Smaller,
        /// <summary>
        /// 大於等於
        /// </summary>
        BiggerEqual,
        /// <summary>
        /// 小於等於
        /// </summary>
        SmallerEqual,
        /// <summary>
        /// 不等於
        /// </summary>
        Unequal,
        /// <summary>
        /// StartWith
        /// </summary>
        StartWith,
        /// <summary>
        /// Contain
        /// </summary>
        Contain,
        /// <summary>
        /// EndWith
        /// </summary>
        EndWith,
    }

    /// <summary>
    /// 排序方式
    /// </summary>
    public enum OrderByType
    {
        /// <summary>
        /// 升序
        /// </summary>
        Asc,
        /// <summary>
        /// 降序
        /// </summary>
        Desc,
    }
}
