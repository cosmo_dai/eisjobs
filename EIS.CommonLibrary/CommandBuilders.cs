﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;

namespace EIS.CommonLibrary
{

    /// <summary>
    /// 執行 SQL 陳述式條件參數物件
    /// </summary>
    public class WhereConditionColumnParameter
    {
        private string column;
        private string name;
        private object value;
        private SQLOperator sqlOper;

        /// <summary>
        /// 執行 SQL 陳述式條件參數物件，若欲使用資料表欄位名稱作為參數名稱，可使用此建購式。欄位運算式為[等於]。
        /// </summary>
        /// <param name="dbColumnName">資料表欄位名稱</param>
        /// <param name="parameterValue">欄位值</param>
        public WhereConditionColumnParameter(string dbColumnName, object parameterValue)
        {
            column = dbColumnName;
            name = dbColumnName;
            value = parameterValue;
            sqlOper = SQLOperator.Equal;
        }
        /// <summary>
        /// 執行 SQL 陳述式條件參數物件，若欲使用資料表欄位名稱作為參數名稱，可使用此建購式。
        /// </summary>
        /// <param name="dbColumnName">資料表欄位名稱</param>
        /// <param name="sqlOperator">欄位運算式</param>
        /// <param name="parameterValue">欄位值</param>
        public WhereConditionColumnParameter(string dbColumnName, SQLOperator sqlOperator, object parameterValue)
        {
            column = dbColumnName;
            name = dbColumnName;
            value = parameterValue;
            sqlOper = sqlOperator;
        }
        /// <summary>
        /// 執行 SQL 陳述式條件參數物件，若欲獨立命名參數名稱，可使用此建購式。欄位運算式為[等於]。
        /// </summary>
        /// <param name="dbColumnName">資料表欄位名稱</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="parameterValue">欄位值</param>
        public WhereConditionColumnParameter(string dbColumnName, string parameterName, object parameterValue)
        {
            column = dbColumnName;
            name = parameterName;
            value = parameterValue;
            sqlOper = SQLOperator.Equal;
        }
        /// <summary>
        /// 執行 SQL 陳述式條件參數物件，若欲獨立命名參數名稱，可使用此建購式。
        /// </summary>
        /// <param name="dbColumnName">資料表欄位名稱</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="sqlOperator">欄位運算式</param>
        /// <param name="parameterValue">欄位值</param>
        public WhereConditionColumnParameter(string dbColumnName, string parameterName, SQLOperator sqlOperator, object parameterValue)
        {
            column = dbColumnName;
            name = parameterName;
            value = parameterValue;
            sqlOper = sqlOperator;
        }

        /// <summary>
        /// 取得資料表欄位名稱
        /// </summary>
        public string ColumnName
        {
            get { return column; }
        }
        /// <summary>
        /// 取得參數名稱
        /// </summary>
        public string ParameterName
        {
            get { return name; }
        }
        /// <summary>
        /// 取得欄位值
        /// </summary>
        public object ParameterValue
        {
            get { return value; }
        }
        /// <summary>
        /// 取得 SQL 欄位運算式
        /// </summary>
        public SQLOperator Operator
        {
            get { return sqlOper; }
        }
    }

    /// <summary>
    /// 欄位物件
    /// </summary>
    public class Column
    {
        private string column;
        private object oldColumnValue;
        private object newColumnValue;

        /// <summary>
        /// 欄位物件
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="oldValue">舊值</param>
        /// <param name="newValue">新值</param>
        public Column(string columnName, object oldValue, object newValue)
        {
            column = columnName.ToUpper();
            oldColumnValue = oldValue;
            newColumnValue = newValue;
        }
        /// <summary>
        /// 欄位物件
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="newValue">新值</param>
        public Column(string columnName, object newValue)
        {
            column = columnName.ToUpper();
            newColumnValue = newValue;
        }

        /// <summary>
        /// 取得欄位名稱
        /// </summary>
        public string ColumnName
        {
            get { return column; }
        }

        /// <summary>
        /// 取得舊值
        /// </summary>
        public object OldValue
        {
            get { return oldColumnValue; }
        }

        /// <summary>
        /// 取得新值
        /// </summary>
        public object NewValue
        {
            get { return newColumnValue; }
        }
    }

    /// <summary>
    /// 自訂 SQL 陳述式物件
    /// </summary>
    public class Command
    {
        private string sql;
        private List<IDbDataParameter> parameters;

        /// <summary>
        /// 自訂 SQL 陳述式物件
        /// </summary>
        /// <param name="commandText">SQL 字串</param>
        /// <param name="parameterList">SQL 參數集合</param>
        public Command(string commandText, List<IDbDataParameter> parameterList)
        {
            sql = commandText;
            if (parameterList == null)
                parameters = new List<IDbDataParameter>();
            else
                parameters = parameterList;
        }

        /// <summary>
        /// 取得 SQL 字串
        /// </summary>
        public string SQL
        {
            get { return sql; }
        }

        /// <summary>
        /// 取得 SQL 參數集合
        /// </summary>
        public List<IDbDataParameter> Parameters
        {
            get { return parameters; }
        }
    }
   
    #region Internal CommandBuilderBase
    public abstract class CommandBuilderBase<CommandBuilder>
        where CommandBuilder : CommandBuilderBase<CommandBuilder>
    {
        //protected DBController dbc;

        protected List<IDbDataParameter> _parameters = new List<IDbDataParameter>();
        protected string _whereClause = string.Empty;

        protected string _tableName = string.Empty;
        private string _functionName = string.Empty;
        private string _pk = string.Empty;
        private string _identify = string.Empty;
        private string _updateUser = string.Empty;
        private DateTime _updateDate = DateTime.Now;
        private List<Column> _logColumns = new List<Column>();
        protected DBAction _dbAction;
        protected DataBaseType _databasetype;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbController">資料庫物件</param>
        /// <param name="targetTable">欲 Update 的資料表名稱</param>
        protected CommandBuilderBase( DBAction action, string targetTable,DataBaseType databaseType)
        {
            _dbAction = action;
            _tableName = targetTable;
            _databasetype = databaseType;
        }

        /// <summary>
        /// 比較操作枚舉轉換為比較操作符
        /// </summary>
        /// <param name="sqlOperator">比較操作符枚舉</param>
        /// <returns></returns>
        protected string ConvertOperator(SQLOperator sqlOperator)
        {
            switch (sqlOperator)
            {
                case SQLOperator.Bigger:
                    return @">";
                case SQLOperator.BiggerEqual:
                    return @">=";
                case SQLOperator.Smaller:
                    return @"<";
                case SQLOperator.SmallerEqual:
                    return @"<=";
                case SQLOperator.Unequal:
                    return @"<>";
                case SQLOperator.StartWith:
                case SQLOperator.EndWith:
                    return @"%";
                case SQLOperator.Contain:
                    return @"%%";
                case SQLOperator.Default:
                    return string.Empty;
                case SQLOperator.Equal:
                default:
                    return @"=";
            }
        }

        /// <summary>
        /// 新增一個條件關係
        /// </summary>
        /// <param name="columnName">DB欄位名稱</param>
        /// <param name="op">比較操作符</param>
        /// <param name="value">欄位值</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> WhereAnd(string columnName, SQLOperator op, object value)
        {
            string tmpColumnName;
            if (op == SQLOperator.StartWith)
                tmpColumnName = ConvertOperator(op) + columnName;
            else
                tmpColumnName = columnName + ConvertOperator(op);

            return WhereAnd( new object[] { tmpColumnName, value });
        }

        /// <summary>
        /// 新增一個條件關係
        /// </summary>
        /// <param name="columnName">DB欄位名稱</param>
        /// <param name="value">欄位值</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> WhereAnd(string columnName, object value)
        {
            return WhereAnd(columnName, SQLOperator.Default, value);
        }

        /// <summary>
        /// 新增一個條件關係
        /// </summary>
        /// <param name="columnName">DB欄位名稱</param>
        /// <param name="op">比較操作符</param>
        /// <param name="value">欄位值</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> WhereOr(DataBaseType dbtype,string columnName, SQLOperator op, object value)
        {
            string tmpColumnName;
            if (op == SQLOperator.StartWith)
                tmpColumnName = ConvertOperator(op) + columnName;
            else
                tmpColumnName = columnName + ConvertOperator(op);

            return WhereOr(dbtype, new object[] { tmpColumnName, value });
        }

        /// <summary>
        /// 新增一個條件關係
        /// </summary>
        /// <param name="columnName">DB欄位名稱</param>
        /// <param name="value">欄位值</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> WhereOr(string columnName, object value)
        {
            return WhereOr(columnName, SQLOperator.Default, value);
        }

        /// <summary>
        /// 新增一組條件欄位及值( AND 關係)
        /// </summary>
        /// <param name="args">DB欄位名稱,欄位值,DB欄位名稱,欄位值,...</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> WhereAnd( params object[] args)
        {
            if (args.Length % 2 > 0)
                throw new Exception("UpdateCommandBuilder.WhereAnd 参数个数不正确, 应为两个值一组.");

            string where;
            where = DBParameterUtility.GenerateWhereAndParameters( ref _parameters,_databasetype, args);
            if (!string.IsNullOrEmpty(where))
            {
                if (_whereClause.Length > 0) _whereClause = string.Format("({0} AND {1})", _whereClause, where);
                else _whereClause = where;
            }
            return this;
        }

        /// <summary>
        /// 新增一個條件欄位及值( OR 關係)
        /// </summary>
        /// <param name="args">DB欄位名稱,欄位值,DB欄位名稱,欄位值,...</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> WhereOr(params object[] args)
        {
            if (args.Length % 2 > 0)
                throw new Exception("UpdateCommandBuilder.WhereOr 参数个数不正确, 应为两个值一组.");

            string where;
            where = DBParameterUtility.GenerateWhereAndParameters(ref _parameters,_databasetype,  args);
            if (!string.IsNullOrEmpty(where))
            {
                if (_whereClause.Length > 0) _whereClause = string.Format("({0} OR {1})", _whereClause, where);
                else _whereClause = where;
            }
            return this;
        }

        /// <summary>
        /// 設置LOG主檔紀錄基本信息
        /// </summary>
        /// <param name="functionName">呼叫交易程式名稱</param>
        /// <param name="targetPK">資料庫主鍵</param>
        /// <param name="targetIdentity">識別碼</param>
        /// <param name="updateUser">資料更新人員</param>
        /// <param name="updateTime">資料更新時間</param>
        /// <returns></returns>
        public CommandBuilderBase<CommandBuilder> SetLogInfo(string functionName, string targetPK, string targetIdentity, string updateUser, DateTime updateTime)
        {
            _functionName = functionName;
            _pk = targetPK;
            _identify = targetIdentity;
            _updateUser = updateUser;
            _updateDate = updateTime;
            return this;
        }

        /// <summary>
        /// 新增一個需要記錄日誌的欄位及欄位值
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="oldValue">欄位舊值</param>
        /// <param name="newValue">欄位新值</param>
        /// <returns></returns>
        protected CommandBuilderBase<CommandBuilder> LogColumn(string columnName, object oldValue, object newValue)
        {
            if (columnName == "CREATE_USER" || columnName == "CREATE_DATE" || columnName == "UPDATE_USER" || columnName == "UPDATE_DATE")
                return this;

            if ((newValue == null || newValue.ToString() == string.Empty) &&
                (oldValue != null && oldValue.ToString() != string.Empty))
                _logColumns.Add(new Column(columnName, oldValue == null || oldValue is DateTime && (DateTime)oldValue == DateTime.MinValue ? DBNull.Value : oldValue,
                                                      newValue == null || newValue is DateTime && (DateTime)newValue == DateTime.MinValue ? DBNull.Value : newValue));
            else if ((newValue != null && newValue.ToString() != string.Empty) &&
                (oldValue == null || oldValue.ToString() == string.Empty))
                _logColumns.Add(new Column(columnName, oldValue == null || oldValue is DateTime && (DateTime)oldValue == DateTime.MinValue ? DBNull.Value : oldValue,
                                                      newValue == null || newValue is DateTime && (DateTime)newValue == DateTime.MinValue ? DBNull.Value : newValue));
            else if (newValue != null && oldValue != null && newValue.ToString() != oldValue.ToString())
                _logColumns.Add(new Column(columnName, oldValue == null || oldValue is DateTime && (DateTime)oldValue == DateTime.MinValue ? DBNull.Value : oldValue,
                                                      newValue == null || newValue is DateTime && (DateTime)newValue == DateTime.MinValue ? DBNull.Value : newValue));

            return this;
        }
        /// <summary>
        /// 新增一组需要記錄日誌的欄位及欄位值
        /// </summary>
        /// <param name="args">欄位名稱,欄位舊值,欄位新值,欄位名稱,欄位舊值,欄位新值,...</param>
        /// <returns></returns>
        //protected CommandBuilderBase<CommandBuilder> LogColumns(params object[] args)
        //{
        //    for (int i = 0; i < args.Length; i += 3)
        //    {
        //        LogColumn(args[i].ToString(), args[i + 1], args[i + 2]);
        //    }

        //    return this;
        //}

        protected List<IDbCommand> GetLogCommands()
        {
            List<IDbCommand> list = new List<IDbCommand>();
            //if (!string.IsNullOrEmpty(_functionName))
            //    list.AddRange(new Log().AddLog(dbc, _functionName, _dbAction, _tableName, _pk, _identify, _logColumns, _updateUser, _updateDate));
            return list;
        }

        /// <summary>
        /// 取得 SQL 陳述式(不包含 LogCommands)
        /// </summary>
        /// <returns></returns>
        public abstract IDbCommand GetCommand();

        /// <summary>
        /// 取得 SQL 陳述式(包含 LogCommands,如果有的話)
        /// </summary>
        /// <returns></returns>
        public List<IDbCommand> GetCommands()
        {
            IDbCommand cmd = GetCommand();
            List<IDbCommand> list = new List<IDbCommand>();
            if (cmd != null)
            {
                list.Add(GetCommand());
                list.AddRange(GetLogCommands());
            }

            return list;
        }

        /// <summary>
        /// 執行交易(執行本CommandBuilder生成的所有Command)
        /// </summary>
        //public void DoTransaction()
        //{
        //    dbc.DoTransaction(GetCommands());
        //}
    }

    #endregion 

    #region InsertCommandBuilder 建立 Insert SQL 陳述式物件
    /// <summary>
    /// 建立 Insert SQL 陳述式物件
    /// </summary>
    public class InsertCommandBuilder : CommandBuilderBase<InsertCommandBuilder>
    {

        public override IDbCommand GetCommand()
        {
            return null;
        }
        /// <summary>
        /// 建立 Insert SQL 陳述式物件
        /// </summary>
        /// <param name="dbController">資料庫物件</param>
        /// <param name="targetTable">欲 Insert 的資料表名稱</param>
        public InsertCommandBuilder(string targetTable, DataBaseType databaseType)
            : base(DBAction.Insert, targetTable, databaseType){}

        

        /// <summary>
        /// 新增要插入的欄位和欄位值
        /// </summary>
        /// <param name="columnName">DB欄位名稱</param>
        /// <param name="value">欄位值</param>
        /// <returns></returns>
        public InsertCommandBuilder InsertColumn(string columnName, object value)
        {
            if (value == null || value.ToString() == string.Empty || value is DateTime && (DateTime)value == DateTime.MinValue)
            {
                _parameters.Add(DBParameterUtility.CreateCommandParameter(columnName, DBNull.Value, _databasetype));
            }
            else
            {
                _parameters.Add(DBParameterUtility.CreateCommandParameter(columnName, value, _databasetype));
            }
            return this;
        }

     

        /// <summary>
        /// 取得 SQL 陳述式(不包含 LogCommands)
        /// </summary>
        /// <returns></returns>
        //public override IDbCommand GetCommand()
        //{
        //    IDbCommand comm = null;

        //    if (_databasetype == DataBaseType.SqlServer)
        //    {
        //        comm = new SqlCommand();
        //    }
        //    //= dbc.CreateCommand();

        //    string sql = " INSERT INTO " + _tableName + " ( ";
        //    string sqlValue = " VALUES ( ";

        //    for (int i = 0; i < _parameters.Count; i++)
        //    {
        //        if (i == _parameters.Count - 1)
        //        {
        //            sql += _parameters[i].ParameterName + " ) ";
        //            sqlValue += ":" + _parameters[i].ParameterName + " ) ";
        //        }
        //        else
        //        {
        //            sql += _parameters[i].ParameterName + ", ";
        //            sqlValue += ":" + _parameters[i].ParameterName + ", ";
        //        }
        //        comm.Parameters.Add(_parameters[i]);// (_parameters[i].ParameterName, _parameters[i].Value);
        //        //dbc.AddCommandParameter(comm.Parameters, _parameters[i].ParameterName, _parameters[i].Value);
        //    }

        //    comm.CommandText = sql;

        //    return comm;
        //}
       
        /// <summary>
        /// 取得 SQL 陳述式(不包含 LogCommands)
        /// </summary>
        /// <returns></returns>
        public Object GetSqlCommand()
        {
            //TO_DATE('03/04/2011 10:07:17', 'MM/DD/YYYY HH24:MI:SS'
            SqlCommand Sqlcmd = new SqlCommand();
            OracleCommand Oraclecmd = new OracleCommand();
            string sql = " INSERT INTO " + _tableName + " ( ";
            string sqlValue = " VALUES ( ";
            string sqlPValue = " VALUES ( ";
            for (int i = 0; i < _parameters.Count; i++)
            {
                if (i == _parameters.Count - 1)
                {
                    sql += _parameters[i].ParameterName + " ) ";
                    if (_databasetype == DataBaseType.SqlServer)
                    {
                        sqlValue += "@" + _parameters[i].ParameterName + " ) ";
                        if (_parameters[i].DbType == DbType.AnsiString)
                        {
                            sqlPValue += "'" + _parameters[i].Value + "' ) ";
                        }
                        else
                        {
                            sqlPValue += "" + _parameters[i].Value + " ) ";
                        }
                    }
                    else
                    {
                        sqlValue += ":" + _parameters[i].ParameterName + " ) ";
                        if (_parameters[i].DbType == DbType.AnsiString)
                        {
                            sqlPValue += "'" + _parameters[i].Value + "' ) ";
                        }
                        else
                        {
                            sqlPValue += "" + _parameters[i].Value + " ) ";
                        }
                    }
                }
                else
                {
                    sql += _parameters[i].ParameterName + ", ";
                    if (_databasetype == DataBaseType.SqlServer)
                    {
                        sqlValue += "@" + _parameters[i].ParameterName + ", ";
                        if (_parameters[i].DbType == DbType.AnsiString)
                        {
                            sqlPValue += "'" + _parameters[i].Value + "', ";
                        }
                        else
                        {
                            sqlPValue += "" + _parameters[i].Value + ", ";
                        }
                    }
                    else
                    {
                        sqlValue += ":" + _parameters[i].ParameterName + ", ";
                        if (_parameters[i].DbType == DbType.AnsiString)
                        {
                            sqlPValue += "'" + _parameters[i].Value + "', ";
                        }
                        else
                        {
                            sqlPValue += "" + _parameters[i].Value + ", ";
                        }
                    }
                }
                if (_databasetype == DataBaseType.SqlServer)
                {
                    Sqlcmd.Parameters.Add(_parameters[i]);
                }
                else
                { 
                    Oraclecmd.Parameters.Add(_parameters[i]);
                }
            }
            if (_databasetype == DataBaseType.SqlServer)
            {
                Sqlcmd.CommandText = sql + sqlValue;
                return Sqlcmd;
            }
            else
            {
                Oraclecmd.CommandText = sql + sqlValue;
                return Oraclecmd;
            }
            
        }
    }
    #endregion

    #region UpdateCommandBuilder 建立 Update SQL 陳述式物件
    /// <summary>
    /// 建立 Update SQL 陳述式物件
    /// </summary>
    public class UpdateCommandBuilder : CommandBuilderBase<UpdateCommandBuilder>
    {
        public override IDbCommand GetCommand()
        {
            return null;
        }
        private StringBuilder updateSets = new StringBuilder();

        /// <summary>
        /// 建立 Update SQL 陳述式物件
        /// </summary>
        /// <param name="dbController">資料庫物件</param>
        /// <param name="targetTable">欲 Update 的資料表名稱</param>
        public UpdateCommandBuilder(string targetTable, DataBaseType databaseType) : base(DBAction.Update, targetTable,databaseType) { }

        /// <summary>
        /// 新增一個更新欄位及值(不會記錄欄位修改日誌)
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="value">欄位值</param>
        /// <returns></returns>
        public UpdateCommandBuilder UpdateColumn(string columnName, object value)
        {
            return UpdateColumn(columnName, "NoOldValue", value);
        }

        /// <summary>
        /// 新增一個更新欄位及值(會記錄欄位修改日誌),當發現新舊值一致時,將不會更新該欄位
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="oldValue">欄位舊值</param>
        /// <param name="newValue">欄位新值</param>
        /// <returns></returns>
        public UpdateCommandBuilder UpdateColumn(string columnName, object oldValue, object newValue)
        {
            string sets;
            base.LogColumn(columnName, oldValue, newValue);
            sets = DBParameterUtility.GenerateUpdateSetsAndParameters(ref _parameters,_databasetype, columnName, oldValue, newValue);
            if (!string.IsNullOrEmpty(sets))
            {
                if (updateSets.Length > 0) updateSets.AppendFormat(",{0}", sets);
                else updateSets.Append(sets);
            }
            return this;
        }
                 

        /// <summary>
        /// 取得 SQL 陳述式
        /// </summary>
        /// <returns></returns>
        public Object GetSqlCommand()
        {
            if (updateSets.Length == 0) return null;

            SqlCommand Sqlcmd = new SqlCommand();
            OracleCommand Oraclecmd = new OracleCommand();
            
            string where = _whereClause.Length > 0 ? " WHERE " + _whereClause : string.Empty;
            string sql = string.Format(" UPDATE {0} SET {1} {2}", _tableName, updateSets.ToString(), where);

            for (int i = 0; i < _parameters.Count; i++)
            {
                if (_databasetype == DataBaseType.SqlServer)
                {
                    Sqlcmd.Parameters.Add(_parameters[i]);
                }
                else
                {
                    Oraclecmd.Parameters.Add(_parameters[i]);
                }
            }
            if (_databasetype == DataBaseType.SqlServer)
            {
                Sqlcmd.CommandText = sql;
                return Sqlcmd;
            }
            else
            {
                Oraclecmd.CommandText = sql;
                return Oraclecmd;
            }
        }
    }

    #endregion

    #region DeleteCommandBuilder 建立 Delete SQL 陳述式物件
    /// <summary>
    /// 建立 Delete SQL 陳述式物件
    /// </summary>
    public class DeleteCommandBuilder : CommandBuilderBase<DeleteCommandBuilder>
    {
        /// <summary>
        /// 建立 Delete SQL 陳述式物件
        /// </summary>
        /// <param name="dbController">資料庫物件</param>
        /// <param name="targetTable">欲 Delete 的資料表名稱</param>
        public DeleteCommandBuilder(string targetTable, DataBaseType databaseType) 
            : base(DBAction.Delete, targetTable,databaseType) { }

        /// <summary>
        /// 新增一個需要記錄日誌的欄位及欄位值
        /// </summary>
        /// <param name="columnName">欄位名稱</param>
        /// <param name="oldValue">欄位舊值</param>
        /// <returns></returns>
        [Obsolete("Delete語句不需要記錄明細日誌,故不需要再調用此方法了")]
        public DeleteCommandBuilder LogColumn(string columnName, object oldValue)
        {
            return base.LogColumn(columnName, oldValue, string.Empty) as DeleteCommandBuilder;
        }

        /// <summary>
        /// 取得 SQL 陳述式
        /// </summary>
        /// <returns></returns>
        public override IDbCommand GetCommand()
        {
            IDbCommand comm = null;

            if (_databasetype == DataBaseType.SqlServer)
            {
                comm = new SqlCommand();
            }
            string where = _whereClause.Length > 0 ? " WHERE " + _whereClause : string.Empty;
            string sql = string.Format(" DELETE {0} {1}", _tableName, where);

            for (int i = 0; i < _parameters.Count; i++)
            {
                comm.Parameters.Add(_parameters[i]);
                //dbc.AddCommandParameter(comm.Parameters, _parameters[i].ParameterName, _parameters[i].Value);
            }
            comm.CommandText = sql;// dbc.GetCommandText(sql, SQLStringType.OracleSQLString);

            return comm;
        }

        /// <summary>
        /// 取得 SQL 陳述式
        /// </summary>
        /// <returns></returns>
        public SqlCommand GetSqlCommand()
        {
            SqlCommand comm = null;

            if (_databasetype == DataBaseType.SqlServer)
            {
                comm = new SqlCommand();
            }
            string where = _whereClause.Length > 0 ? " WHERE " + _whereClause : string.Empty;
            string sql = string.Format(" DELETE {0} {1}", _tableName, where);

            for (int i = 0; i < _parameters.Count; i++)
            {
                comm.Parameters.Add(_parameters[i]);
            }
            comm.CommandText = sql;

            return comm;
        }
    }

    #endregion
}
