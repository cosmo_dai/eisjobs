﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.Data;
using System.Configuration;

namespace EIS.CommonLibrary
{
    public class DataBaseObject
    {
        public DataBaseType databaseType;

        public OracleConnection OracleConn;

        public OracleTransaction OracleTran;

        public SqlConnection SqlConn;

        public SqlTransaction SqlTran;


        public DataBaseObject(DataBaseType dbtype) {
            databaseType = dbtype;
        }

        public void OpenConnect(string connString)
        {
            if (databaseType == DataBaseType.Oracle)
            {
                OracleConn = new OracleConnection(connString);
                OracleConn.Open();
            }
            else
            {
                SqlConn = new SqlConnection(connString);                
                SqlConn.Open();
            }
        }

        public void BeginTransaction()
        {
            if (databaseType == DataBaseType.Oracle)
            {
                OracleTran = OracleConn.BeginTransaction();
            }
            else
            {
                SqlTran = SqlConn.BeginTransaction();
            }
        }

        public void Commit()
        {
            if (databaseType == DataBaseType.Oracle)
            {
                OracleTran.Commit();
            }
            else
            {
                SqlTran.Commit();
            }
        }

        public void Rollback()
        {
            if (databaseType == DataBaseType.Oracle)
            {
                OracleTran.Rollback();
            }
            else
            {
                SqlTran.Rollback();
            }
        }

        public void ColseConnect()
        {
            if (OracleConn != null)
            {
                OracleConn.Close();
                OracleConn.Dispose();
            }

            if (SqlConn != null)
            {
                SqlConn.Close();
                SqlConn.Dispose();
            }
        }

        public int ExcuteSql(List<string> sqls)
        {
            int Result = 0;
            if (databaseType == DataBaseType.SqlServer)
            {
                for (int i = 0; i < sqls.Count; i++)
                {
                    SqlCommand cmd = new SqlCommand(sqls[i]);
                    Result += ExcuteSqlCommand(cmd);
                }
                return Result;
            }
            else
            {
                for (int i = 0; i < sqls.Count; i++)
                {
                    OracleCommand cmd = new OracleCommand(sqls[i]);
                    Result += ExcuteOracleSqlCommand(cmd);
                }
                return Result;
            }
        }
        public int ExcuteSql(string sql)
        {
            if (databaseType == DataBaseType.SqlServer)
            {
                SqlCommand cmd = new SqlCommand(sql);
                return ExcuteSqlCommand(cmd);
            }
            else
            {
                OracleCommand cmd = new OracleCommand(sql);
                return ExcuteOracleSqlCommand(cmd);
            }
        }

        public int ExcuteCommand(Object ob)
        {
            if (databaseType == DataBaseType.SqlServer)
            {
                return ExcuteSqlCommand((SqlCommand)ob);
            }
            else
            {
                return ExcuteOracleSqlCommand((OracleCommand)ob);
            }
        }
        public int ExcuteCommand(List<Object> ob)
        {
            int Result = 0;
            if (databaseType == DataBaseType.SqlServer)
            {
                for (int i = 0; i < ob.Count; i++)
                {
                    Result += ExcuteSqlCommand((SqlCommand)ob[i]);
                }
                return Result;
            }
            else
            {
                for (int i = 0; i < ob.Count; i++)
                {
                    Result += ExcuteOracleSqlCommand((OracleCommand)ob[i]);
                }
                return Result;
            }
        }

        private int ExcuteSqlCommand(SqlCommand cmd)
        {
            cmd.Connection = SqlConn;
            cmd.Transaction = SqlTran;
            return cmd.ExecuteNonQuery();
        }

        private int ExcuteOracleSqlCommand(OracleCommand cmd)
        {
            cmd.Connection = OracleConn;
            cmd.Transaction = OracleTran;
            return cmd.ExecuteNonQuery();
        }

        public object ExcuteScalar(string sql)
        {
            if (databaseType == DataBaseType.SqlServer)
            {
                SqlCommand cmd = new SqlCommand(sql);
                return ExcuteSqlScalar(cmd);
            }
            else
            {
                OracleCommand cmd = new OracleCommand(sql);
                return ExcuteOracleScalar(cmd);
            }
        }
        private object ExcuteSqlScalar(SqlCommand cmd)
        {
            cmd.Connection = SqlConn;
            cmd.Transaction = SqlTran;
            object oo = cmd.ExecuteScalar();
            return oo == null ? "" : oo;
        }

        private object ExcuteOracleScalar(OracleCommand cmd)
        {
            cmd.Connection = OracleConn;
            cmd.Transaction = OracleTran;
            object oo = cmd.ExecuteOracleScalar();
            return oo == null ? "" : oo;
        }

        public DataView GetData(string sql)
        {
            if (databaseType == DataBaseType.Oracle)
            {
                return GetOracleData(sql);
            }
            else
            {
                return GetSqlServerData(sql);
            }
        }
        private DataView GetSqlServerData(string sql)
        {
            SqlCommand cmd = SqlConn.CreateCommand();

            try
            {

                DataTable dt = new DataTable();
                cmd.CommandText = sql;
                cmd.Transaction = SqlTran;
                cmd.CommandTimeout = 300;
                SqlDataReader dataReader = cmd.ExecuteReader();
                
                dt.Load(dataReader);

                return new DataView(dt);
            }
            catch (Exception E)
            {
                throw E;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }
        private DataView GetOracleData( string sql)
        {
            OracleCommand cmd = OracleConn.CreateCommand();

            try
            {
                DataTable dt = new DataTable();
                cmd.CommandText = sql;
                cmd.Transaction = OracleTran;
                OracleDataReader dataReader = cmd.ExecuteReader();
                
                dt.Load(dataReader);

                return new DataView(dt);
            }
            catch (Exception E)
            {
                throw E;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public DataTable GetDataTable(string sql)
        {
            if (databaseType == DataBaseType.Oracle)
            {
                return GetOracleDataTable(sql);
            }
            else
            {
                return GetSqlServerDataTable(sql);
            }
        }

        private DataTable GetSqlServerDataTable(string sql)
        {
            SqlCommand cmd = SqlConn.CreateCommand();

            try
            {

                DataTable dt = new DataTable();
                cmd.CommandText = sql;
                cmd.Transaction = SqlTran;
                SqlDataReader dataReader = cmd.ExecuteReader();

                dt.Load(dataReader);

                return dt;
            }
            catch (Exception E)
            {
                throw E;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        private DataTable GetOracleDataTable(string sql)
        {
            OracleCommand cmd = OracleConn.CreateCommand();

            try
            {
                DataTable dt = new DataTable();
                cmd.CommandText = sql;
                cmd.Transaction = OracleTran;
                OracleDataReader dataReader = cmd.ExecuteReader();
                dt.Load(dataReader);
                return dt;
            }
            catch (Exception E)
            {
                throw E;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

    }
}
