﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OracleClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;

namespace Genesis.Gtimes.EIS
{
    public class EISBase
    {
        #region private variable
 
        protected string Full_DateTime_Format = "yyyyMMddHHmmss.fffffff";

        /// <summary>
        /// EIS轉換使用者
        /// </summary>
        protected string WriteUserID = "EISLoader";

        /// <summary>
        /// Transfer轉換源
        /// </summary>
        protected DataBaseObject dboEIS;

        /// <summary>
        /// Source來源
        /// </summary>
        protected DataBaseObject dboERP;

        /// <summary>
        /// Purpose目的源
        /// </summary>
        protected DataBaseObject dboMES;

        #endregion 
                
        #region public Log

        /// <summary>
        /// Log物件
        /// </summary>
        protected Logger nlog;

        /// <summary>
        /// 記錄一般作業Log
        /// </summary>
        /// <param name="Message"></param>
        protected void TraceNormalLog(string Message)
        {
            nlog.Info(Message);
        }

        /// <summary>
        /// 記錄錯誤log
        /// </summary>
        /// <param name="ex"></param>
        protected void TraceErrorLog(Exception ex)
        {
            nlog.Error(GetExceptionAllDetails(ex));
        }

        protected string GetExceptionAllDetails(Exception ex)
        {
            string exMessage = ex.Message;
            while (ex.InnerException != null)
            {
                if (exMessage.IndexOf(ex.InnerException.Message) == -1)
                    exMessage += ">>" + ex.InnerException.Message;
                ex = ex.InnerException;
            };
            return exMessage;
        }

        #endregion 
        
        public void CallLogTxt(string path)
        {
            System.Diagnostics.Process.Start(path);
        }

        /// <summary>
        /// 連線資料庫
        /// </summary>
        protected void ConnectDataBase()
        {
            #region ERP

            DataBaseType ERPdatabaseType = Commfunction.ConvertToDataBaseType(ConfigurationManager.AppSettings["ERP.DBType"].ToString().Trim());
            string ERPConnString = "";
            if (ERPdatabaseType == DataBaseType.SqlServer)
            {
                ERPConnString = ConfigurationManager.ConnectionStrings["ERP.SqlServer"].ToString();
            }
            else
            {
                ERPConnString = ConfigurationManager.ConnectionStrings["ERP.Oracle"].ToString();
            }
            dboERP = new DataBaseObject(ERPdatabaseType);
            dboERP.OpenConnect(ERPConnString);

            Console.WriteLine("Connect ERP OK !");
            #endregion

            #region EIS Connection

            DataBaseType EISdatabaseType = Commfunction.ConvertToDataBaseType(ConfigurationManager.AppSettings["EIS.DBType"].ToString().Trim());
            string EISConnString = "";
            if (EISdatabaseType == DataBaseType.SqlServer)
            {
                EISConnString = ConfigurationManager.ConnectionStrings["EIS.SqlServer"].ToString();
            }
            else
            {
                EISConnString = ConfigurationManager.ConnectionStrings["EIS.Oracle"].ToString();
            }
            dboEIS = new DataBaseObject(EISdatabaseType);
            dboEIS.OpenConnect(EISConnString);
            //dboEIS.BeginTransaction();

            Console.WriteLine("Connect EIS OK !");
            #endregion

            #region MES
            DataBaseType MESdatabaseType = Commfunction.ConvertToDataBaseType(ConfigurationManager.AppSettings["MES.DBType"].ToString().Trim());
            string MESConnString = "";
            if (MESdatabaseType == DataBaseType.SqlServer)
            {
                MESConnString = ConfigurationManager.ConnectionStrings["MES.SqlServer"].ToString();
            }
            else
            {
                MESConnString = ConfigurationManager.ConnectionStrings["MES.Oracle"].ToString();
            }
            dboMES = new DataBaseObject(MESdatabaseType);
            dboMES.OpenConnect(MESConnString);
            //dboMES.BeginTransaction();

            Console.WriteLine("Connect MES OK !");
            #endregion
        } 

    }
}
