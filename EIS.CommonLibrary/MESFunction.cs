﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Genesis.Gtimes.AlrLibrary;
using System.IO;

namespace EIS.CommonLibrary
{
    public partial class MESFunction
    {
        DataBaseObject _dbo;

        public MESFunction(DataBaseObject dbo)
        {
            _dbo = dbo;
        }

        #region IsExist

        public bool IsExistUserNo(string accountNo)
        {
            string sql = " SELECT Count(1) FROM AD_USER WHERE ACCOUNT_NO = '{0}'";
            sql = string.Format(sql, accountNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistVendorNo(string vendorNo)
        {
            string sql = " SELECT Count(1) FROM PF_VENDOR WHERE VENDOR_NO = '{0}'";
            sql = string.Format(sql, vendorNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistRoute(string route_ver_sid)
        {
            string sql = "SELECT Count(1) FROM PF_ROUTE_VER where CONVERT(varchar(100), EXPIRYDATE, 111)<getdate() and VERSION_STATE='Enable' and ROUTE_VER_SID='{0}'";
            sql = string.Format(sql, route_ver_sid.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistCustomerNo(string customerNo)
        {
            string sql = " SELECT Count(1) FROM PF_CUSTOMER WHERE CUSTOMER_NO = '{0}'";
            sql = string.Format(sql, customerNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistPartName(string partName)
        {
            string sql = " SELECT Count(1) FROM PF_PARTNO WHERE PART_NAME = '{0}'";
            sql = string.Format(sql, partName.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistPartNo(string partNo)
        {            
            string sql = " SELECT Count(1) FROM PF_PARTNO WHERE PARTNO = '{0}'";
            sql = string.Format(sql, partNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistMLot(string mtr_lot)
        {
            string sql = " SELECT Count(1) FROM MT_LOT WHERE MTR_LOT = '{0}'";
            sql = string.Format(sql, mtr_lot.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistWO(string wo)
        {           
            string sql = " SELECT Count(1) FROM WP_WO WHERE WO = '{0}'";
            sql = string.Format(sql, wo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }
 
        public bool IsExistSO(string so)
        {
            string sql = " SELECT Count(1) FROM WP_SO WHERE SO = '{0}' ";
            sql = string.Format(sql, so.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistSOPartNo(string so, string partNo)
        {
            string sql = " SELECT Count(1) FROM WP_SO_PARTNO WHERE SO = '{0}' and PARTNO ='{1}' ";
            sql = string.Format(sql, so.Trim(), partNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistShipNo(string shipNo)
        {
            string sql = " SELECT Count(1) FROM SH_SHIP WHERE SHIP_NO = '{0}' ";
            sql = string.Format(sql, shipNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistShipPartNo(string shipNo, string partNo,string seq)
        {
            string sql = " SELECT Count(1) FROM SH_SHIP_DETAIL WHERE SHIP_NO = '{0}' and PARTNO ='{1}' AND SEQ='{2}' ";
            sql = string.Format(sql, shipNo.Trim(), partNo.Trim(), seq.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistBomNo(string bomNo)
        {
            string sql = " SELECT Count(1) FROM ZZ_BOM WHERE BOM_NO = '{0}' ";
            sql = string.Format(sql, bomNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistBomItem(string bomNo)
        {
            string sql = " SELECT Count(1) FROM ZZ_BOM_ITEM WHERE BOM_NO = '{0}' ";
            sql = string.Format(sql, bomNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistCustomerPartNo(string customerNo, string partNo)
        {
            string sql = " SELECT Count(1) FROM ZZ_PF_CUSTOMER_PARTNO WHERE CUSTOMER_NO = '{0}' and PARTNO ='{1}' ";
            sql = string.Format(sql, customerNo.Trim(), partNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistPartAbout(string partNo, string mtrPartNo)
        {
            string sql = " SELECT Count(1) FROM PF_PARTNO_ABOUT WHERE PARTNO = '{0}' and MTR_PARTNO ='{1}' ";
            sql = string.Format(sql, partNo.Trim() ,mtrPartNo.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }


        public bool IsExistStock(string pdno)
        {
            string sql = " SELECT Count(1) FROM APS_apiprodh WHERE PDNO = '{0}' ";
            sql = string.Format(sql, pdno.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        public bool IsExistStockDetail(string pdno, string seq)
        {
            string sql = " SELECT Count(1) FROM APS_apiprodi WHERE PDNO = '{0}' and SORTNO ='{1}'";
            sql = string.Format(sql, pdno.Trim(), seq.Trim());
            int iCount = Convert.ToInt32(_dbo.ExcuteScalar(sql).ToString());
            bool flag = (iCount > 0);
            return flag;
        }

        #endregion 

        #region GetSID

        /// <summary>
        /// 查工單是否已經Release
        /// </summary>
        public string GetWOStatus(string wo)
        {
            string sql = " SELECT STATUS FROM WP_WO WHERE WO = '{0}' ";
            sql = string.Format(sql, wo);
            string str = _dbo.ExcuteScalar(sql).ToString();
            return str;
        }

        public string GetMLOTStatus(string mtr_lot)
        {
            string sql = " SELECT STATUS FROM MT_LOT WHERE MTR_LOT = '{0}' ";
            sql = string.Format(sql, mtr_lot);
            string str = _dbo.ExcuteScalar(sql).ToString();
            return str;
        }

        public string GetWOSID(string wo)
        {
            string sql = " SELECT WO_SID FROM WP_WO WHERE WO = '{0}'";
            sql = string.Format(sql, wo);
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }

        public string GetPartNoSID(string partNo)
        {
            string sql = " SELECT PARTNO_SID FROM PF_PARTNO WHERE PARTNO = '{0}'";
            sql = string.Format(sql, partNo);
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }
         
        public string GetCustomerSID(string customerNo)
        {
            string sql = " SELECT CUSTOMER_SID FROM PF_CUSTOMER WHERE CUSTOMER_NO = '{0}'";
            sql = string.Format(sql, customerNo);
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }

        public string GetCustPartSID(string customerNo, string partNo)
        {
            string sql = " SELECT CUST_PARTNO_SID FROM ZZ_PF_CUSTOMER_PARTNO WHERE CUSTOMER_NO = '{0}' and PARTNO ='{1}' ";
            sql = string.Format(sql, customerNo.Trim(), partNo.Trim());
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }

        public string GetSOSID(string so)
        {
            string sql = " SELECT SO_SID FROM WP_SO WHERE SO = '{0}'";
            sql = string.Format(sql, so);
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }

        public string GetShipSID(string shipNo)
        {
            string sql = " SELECT SHIP_SID FROM SH_SHIP WHERE SHIP_NO = '{0}'";
            sql = string.Format(sql, shipNo);
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }

        public string GetBomSID(string bomNo)
        {
            string sql = " SELECT BOM_SID FROM ZZ_BOM WHERE BOM_NO = '{0}'";
            sql = string.Format(sql, bomNo);
            string SID = _dbo.ExcuteScalar(sql).ToString();
            return SID;
        }


        #endregion

        #region GetData

        public static string WriteAlarmJob(DataBaseObject dbo, string SendSubject, string SendFilePath)
        {
            DateTime SendTime = DateTime.Now;
            string SendType = "MAIL";
            //string SendSubject = "[EIS]轉檔錯誤通知--工單";            
            //string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            //string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;
            
            string SendBody = "轉檔Log記錄,請詳閱附件檔案!!";

            #region 轉檔錯誤通知群組之User

            MESFunction mes = new MESFunction(dbo);
            DataView dvFailUser = mes.GetEISFailUser();
            List<string> SendList = new List<string>();

            for (int i = 0; i < dvFailUser.Count; i++)
            {
                SendList.Add(dvFailUser[i]["EMAIL"].ToString());
            }

            #endregion

            byte[] SendFileArray = new byte[0];
            if (SendFilePath != string.Empty)
            {
                FileStream _fileStream = new FileStream(SendFilePath, FileMode.Open, FileAccess.Read);
                _fileStream.Position = 0;
                SendFileArray = new byte[_fileStream.Length];
                _fileStream.Read(SendFileArray, 0, (int)_fileStream.Length);
            }

            string RtMsg = AlarmJobInsert.Execute(SendType, SendTime, SendList, SendSubject, SendBody, SendFilePath, SendFileArray);

            return RtMsg;
        }

        /// <summary>
        /// 取出 ｢轉檔錯誤通知｣ 群組User
        /// </summary>
        /// <returns></returns>
        public DataView GetEISFailUser()
        {
            string sql = @"
                Select A.GROUP_NAME,C.USER_NAME,C.USER_EN_NAME,C.NICKNAME,C.SEX,C.ACCOUNT_NO ,C.EMP_NO,C.EMAIL
                From AD_USERGROUP A, AD_USERGROUP_USER_LIST B, AD_USER C
                Where A.GROUP_SID=B.GROUP_SID 
                  AND B.USER_SID=C.USER_SID  
                  AND A.GROUP_SID='GTI11090714143783992' ";           
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetVendorData(string vendorNo)
        {
            string sql = "SELECT VENDOR_SID,VENDOR_NO,VENDOR_NAME FROM PF_VENDOR WHERE VENDOR_NO = '{0}'";
            sql = string.Format(sql, vendorNo);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetCustomerData(string customerNo)
        {
            string sql = "SELECT CUSTOMER_SID,CUSTOMER_NO,CUSTOMER FROM PF_CUSTOMER WHERE CUSTOMER_NO = '{0}'";
            sql = string.Format(sql, customerNo);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetPartTypeData()
        {
            string sql = @" SELECT PARTNO_TYPE_SID  FROM PF_PARTNO_TYPE WHERE PARTNO_TYPE_NO = 'PartType01' ";            
            DataView dv = _dbo.GetData(sql);
            return dv;
        }


        public DataView GetPartTypeDataByNO(string PartType)
        {
            string sql = @" SELECT PARTNO_TYPE_SID  FROM PF_PARTNO_TYPE WHERE PARTNO_TYPE_NO = '{0}' ";
            sql = string.Format(sql, PartType);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetProductData()
        {
            string sql = @" SELECT PRODUCT_SID  FROM PF_PRODUCT WHERE PRODUCT_NO = 'Product' ";
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetPartNoData(string partNo)
        {
            string sql = @"SELECT A.PARTNO_SID,A.PARTNO,A.PART_NAME,B.PARTNO_VER_SID,B.VERSION,A.PRODUCT_SID,A.UNIT,B.MANUFACTURE_FALG
                            FROM PF_PARTNO A,PF_PARTNO_VER B
                            WHERE A.PARTNO_SID = B.PARTNO_SID
                            AND A.DEFAULT_VERSION = B.VERSION
                            AND A.PARTNO = N'{0}'";
            sql = string.Format(sql, partNo);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetPartNoDataByName(string partName)
        {
            string sql = @"SELECT A.PARTNO_SID,A.PARTNO, A.PART_NAME, A.VERSION_SPEC
                            FROM PF_PARTNO A 
                            WHERE A.PARTNO = N'{0}'";
            sql = string.Format(sql, partName);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetPartNoDefaultRouteData(string partNoVerSID)
        {
            string sql = @"SELECT A.PARTNO_VER_SID,
                               A.ROUTE_SID,
                               A.ROUTE_NO,
                               A.ROUTE_NAME,
                               A.DEFAULT_FLAG,
                               B.ROUTE_VER_SID,
                               B.VERSION,
                               B.VERSION_STATE
                          FROM PF_PARTNO_ROUTEVER A, PF_ROUTE_VER B
                         WHERE     A.DEFAULT_FLAG = 'T'
                               AND A.ROUTE_SID = B.ROUTE_SID
                               AND B.DEFAULT_FLAG = 'T'
                               AND A.PARTNO_VER_SID = N'{0}' ";
            sql = string.Format(sql, partNoVerSID);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetProductData(string productSID)
        {
            string sql = @"SELECT  PRODUCT_SID, PRODUCT_NO, PRODUCT, 
                                   DESCRIPTION, CREATE_USER, CREATE_DATE, 
                                   UPDATE_USER, UPDATE_DATE
                            FROM PF_PRODUCT
                            WHERE PRODUCT_SID = N'{0}'";
            sql = string.Format(sql, productSID);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }

        public DataView GetMLotData(string mtr_lot)
        {
            string sql = " SELECT * FROM MT_LOT WHERE MTR_LOT = '{0}' ";
            sql = string.Format(sql, mtr_lot);
            DataView dv = _dbo.GetData(sql);
            return dv;
        }
        #endregion
    }
}
