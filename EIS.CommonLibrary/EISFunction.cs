﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.Data;

namespace EIS.CommonLibrary
{
    /// <summary>
    /// Log 工作項目狀態
    /// </summary>
    public enum LogState
    {
        /// <summary>
        /// 一般
        /// </summary>
        Normal,
        /// <summary>
        /// 錯誤
        /// </summary>
        Error,
    }

    /// <summary>
    /// Log 工作項目執行動作
    /// </summary>
    public enum LogAction
    {
        /// <summary>
        /// 新增
        /// </summary>
        Insert,
        /// <summary>
        /// 修改
        /// </summary>
        Update,
        /// <summary>
        /// 當 JOB 狀態為 Run, 表示有平行處理的問題, 不更新 EIS_JOBCONTROL.JOB_STATE 
        /// </summary>
        SkipJob,
        /// <summary>
        /// 資料同步
        /// </summary>
        DataSync,
        /// <summary>
        /// 第一次資料同步
        /// </summary>
        FirstSync,
    }

    /// <summary>
    /// JOB狀態
    /// </summary>
    public enum JobState
    {
        /// <summary>
        /// 可進行轉檔工作, 同時無平行處理
        /// </summary>
        Idle,
        /// <summary>
        /// 不可進行轉檔工作, 同時有平行處理中
        /// </summary>
        Run, 
    }
    
    /// <summary>
    /// EIS_JOBCONTROL 資料
    /// </summary>
    public struct EISResult
    {
        /// <summary>
        /// 最後一次執行的條件(記錄於EIS_JOBCONTROL.CONTROL_VALUE)
        /// </summary>
        public string[] Condition;

        /// <summary>
        /// 判別執行緒是否並行
        /// </summary>
        public bool multiProcess;

        /// <summary>
        /// 最後一次執行的時間
        /// </summary>
        public DateTime EISOldLastUpdate;
    }

    public static partial class EISFunction
    {
        /// <summary>
        /// 取得EIS Job執行狀況
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="EIS_SID"></param>
        /// <param name="Condition">最後一次執行的條件</param>
        /// <param name="EISOldLastUpdate">最後一次執行的時間</param>
        /// <returns></returns>
        public static EISResult CheckEISResult(DataBaseObject dbo, string EIS_SID, string[] Condition, DateTime OldLastUpdate)
        {
            bool multiProcess = false;

            string sEIS_Query = "SELECT CONTROL_VALUE, JOB_STATE, LASTUPDATE FROM EIS_JOBCONTROL WHERE EIS_SID = '" + EIS_SID + "'";

            DataView dvEISJob = dbo.GetData(sEIS_Query);
            
            if (dvEISJob != null && dvEISJob.Count > 0)
            {
                #region 檢查是否並行衝突

                // 取得最後一次執行的條件
                // state=IDLE 代表可以進行更新
                // state=RUN  代表有同樣程式執行中，不可以再進行
                
                string Job_State = dvEISJob[0]["JOB_STATE"].ToString();

                if (Job_State == JobState.Idle.ToString().ToUpper())
                {

                    Condition = dvEISJob[0]["CONTROL_VALUE"].ToString().Split(';');
                    OldLastUpdate = Convert.ToDateTime(dvEISJob[0]["LASTUPDATE"]);

                    // 如果狀況=IDLE，代表可以進行更新，並更新SID狀況為 RUN
                    // 並檢查並行衝突
                    int iUpdateCount = UpdateJobControl(dbo, EIS_SID, JobState.Run, OldLastUpdate);
                    if (iUpdateCount < 0) multiProcess = true;
                }
                else
                {
                    //有同樣程式執行中,不可以在進行
                    multiProcess = true;
                }

                #endregion
            }
            else
            {                
                #region 表示為第一次執行，新增一筆資料狀況為 RUN

                int iInsertCount = CreateJobControl(dbo, EIS_SID, JobState.Run, OldLastUpdate);
                if (iInsertCount < 0) multiProcess = true;

                #endregion
            }            
            //
            //
            EISResult ReturnValue;
            if (Condition != null)
            {
                if (string.IsNullOrEmpty(Condition[0].Trim()) == true) Condition = null;
            }
            ReturnValue.multiProcess = multiProcess;            
            ReturnValue.Condition = Condition;
            ReturnValue.EISOldLastUpdate = OldLastUpdate;

            return ReturnValue;
        }

        /// <summary>
        /// 新增EIS Job執行狀況
        /// </summary>
        public static int CreateJobControl(DataBaseObject dbo, string EIS_SID, JobState Job_State, DateTime OldLastUpdate)
        {
            // 新增資料表名稱
            string tableName = "EIS_JOBCONTROL";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);
 
            // 對應欄位            
            icb.InsertColumn("EIS_SID", EIS_SID);
            //if (Control_Value != null) icb.InsertColumn("CONTROL_VALUE", Control_Value);
            icb.InsertColumn("JOB_STATE", Job_State.ToString().ToUpper());
            icb.InsertColumn("LASTUPDATE", OldLastUpdate);

            int iCount = dbo.ExcuteCommand(icb.GetSqlCommand());

            return iCount;
        }

        /// <summary>
        /// 更新EIS Job執行狀況
        /// </summary>        
        public static int UpdateJobControl(DataBaseObject dbo, string EIS_SID, JobState Job_State, string Control_Value, DateTime OldLastUpdate)
        {
            // 異動資料表名稱
            string tableName = "EIS_JOBCONTROL";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);
             
            // 建立異動條件
            ucb.WhereAnd("EIS_SID", EIS_SID);
            ucb.WhereAnd("LASTUPDATE", OldLastUpdate);

            // 解析對應欄位 
            ucb.UpdateColumn("JOB_STATE", Job_State.ToString().ToUpper());
            if (Job_State != JobState.Run) ucb.UpdateColumn("LASTUPDATE", System.DateTime.Now);
            if (Control_Value != null) ucb.UpdateColumn("CONTROL_VALUE", Control_Value);
            
            int iCount = dbo.ExcuteCommand(ucb.GetSqlCommand());

            return iCount;
        }       
        public static int UpdateJobControl(DataBaseObject dbo, string EIS_SID, JobState Job_State, DateTime OldLastUpdate)
        {
            return UpdateJobControl(dbo, EIS_SID, Job_State, null, OldLastUpdate);
        }

        /// <summary>
        /// 新增Log記錄
        /// </summary>
        public static int CreateLog(DataBaseObject dbo, DateTime Job_StartTime, LogState Log_State, LogAction Log_Action, int Row_Count, string Job_Desc)
        {
            // 新增資料表名稱
            string tableName = "EIS_JOBHIST";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);
             
            // 對應欄位 
            icb.InsertColumn("EIS_SID", Guid.NewGuid().ToString());
            icb.InsertColumn("START_TIME", Job_StartTime);
            icb.InsertColumn("END_TIME", System.DateTime.Now);
            icb.InsertColumn("JOB_STATE", Log_State.ToString());
            icb.InsertColumn("JOB_ACTION", Log_Action.ToString());
            icb.InsertColumn("ROW_COUNT", Row_Count);
            icb.InsertColumn("JOB_DESC", Job_Desc);

            int iCount = dbo.ExcuteCommand(icb.GetSqlCommand());

            return iCount;
        }

        /// <summary>
        /// 新增Log記錄
        /// </summary>
        public static int CreateLog(DataBaseObject dbo, string Job_StartTime, string Job_State, string Job_Action, int Row_Count, string Job_Desc)
        {
            string SID = Guid.NewGuid().ToString();

            string Job_EndTime = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
            INSERT INTO EIS_JOBHIST
            ( EIS_SID, START_TIME, END_TIME, JOB_STATE, JOB_ACTION, ROW_COUNT, JOB_DESC) 
            VALUES
            ( :EIS_SID, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}') "
            , SID
            , Job_StartTime
            , Job_EndTime
            , Job_State
            , Job_Action
            , Row_Count
            , Job_Desc
            );

            int iCount = dbo.ExcuteSql(sql.ToString());

            return iCount;
        }

        /// <summary>
        /// 更新EIS Job執行狀況
        /// </summary>        
        public static int UpdateJobControl(DataBaseObject dbo, string EIS_SID, string Job_State, string Control_Value, DateTime OldLastUpdate)
        {            
            StringBuilder sql = new StringBuilder();

            string LastUpdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");

            sql.AppendFormat(@"
            UPDATE EIS_JOBCONTROL 
            SET CONTROL_VALUE = '{2}', JOB_STATE = '{3}', LASTUPDATE = '{4}'
            WHERE EIS_SID = '{0}' AND LASTUPDATE = '{1}' 
            ", EIS_SID, OldLastUpdate.ToString("yyyy-MM-dd HH:mm:ss.fff"), Control_Value, Job_State, LastUpdate);

            int iCount = dbo.ExcuteSql(sql.ToString());

            return iCount;
        }
         
        public static int EISLOGInsert(DataBaseObject dbo, string EIS_SID, string Job_StartTime, string Job_EndTime, string Job_State, string Job_Action, int Row_Count, string Job_Desc)
        {
            StringBuilder sql = new StringBuilder();

            sql.AppendFormat(@"
            INSERT INTO EIS_JOBHIST
            ( EIS_SID, START_TIME, END_TIME, JOB_STATE, JOB_ACTION, ROW_COUNT, JOB_DESC) 
            VALUES
            ( '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}') "
            , EIS_SID
            , DateTime.ParseExact(Job_StartTime, "yyyyMMddHHmmss", null).ToString("yyyy-MM-dd HH:mm:ss")
            , DateTime.ParseExact(Job_EndTime, "yyyyMMddHHmmss", null).ToString("yyyy-MM-dd HH:mm:ss")            
            , Job_State
            , Job_Action
            , Row_Count
            , Job_Desc 
            );
            
            //SqlCommand EISHisInsertCmd = new SqlCommand(EISInsertSql, EISModifyConn);
            //int Count = System.Convert.ToInt32(EISHisInsertCmd.ExecuteNonQuery());
            //dbo.Commit();

            int Count = dbo.ExcuteSql(sql.ToString());
            
            return Count;
        }
       
        public static int EIS_JobControl_Update(DataBaseObject dbo, string EIS_SID, string Job_State, string JobControlValue, DateTime EISOldLastUpdate)
        {
            string sql = "Update EIS_JOBCONTROL set CONTROL_VALUE='" + JobControlValue + "', JOB_STATE='" + Job_State + "',LASTUPDATE='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "' WHERE EIS_SID='" + EIS_SID + "' AND LASTUPDATE='" + EISOldLastUpdate.ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";
            
            int Count = dbo.ExcuteSql(sql);// System.Convert.ToInt32(EISHisInsertCmd.ExecuteNonQuery());

            //SqlCommand EISHisInsertCmd = new SqlCommand(EISSql, EISModifyConn, tran);
            //int Count = System.Convert.ToInt32(EISHisInsertCmd.ExecuteNonQuery());
            //dbo.Commit();
            
            return Count;
        }
     
    }
}
