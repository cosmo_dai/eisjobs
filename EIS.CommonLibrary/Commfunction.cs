﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.Data;

namespace EIS.CommonLibrary
{   
    public static partial class Commfunction
    {
        public static string StringRemoveEmptyChar(string value)
        {
            string ReturnValue = "";
            value = value.Trim();
            value = value.Replace(" ", "");
            value = value.Replace("\r", "");
            value = value.Replace("\n", "");
            value = value.Replace("\t", "");
            for (int m = 0; m < value.Length; m++)
            {
                if (value[m].ToString() == " ")
                {
                    continue;
                }
                else if (Convert.ToInt32(value[m]) == 12288)
                {
                    continue;
                }
                else
                {
                    ReturnValue += value[m].ToString();
                }
            }
            return ReturnValue;
        }


        public static Decimal ConvertToDecimal(string value)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch
            {
                return 0;
            }
        }
        //public static DataView GetSqlServerData(SqlConnection conn,string sql)
        //{
        //    SqlCommand cmd = conn.CreateCommand();
           
        //    try
        //    {
              
        //        DataTable dt = new DataTable();
        //        cmd.CommandText = sql;
        //        SqlDataReader dataReader = cmd.ExecuteReader();


        //        dt.Load(dataReader);

        //        return new DataView(dt);
        //    }
        //    catch (Exception E)
        //    {
        //        throw E;
        //    }
        //    finally
        //    {
        //        if (cmd != null)
        //        {
        //            cmd.Dispose();
        //        }
        //    }
        //}

        //public static DataView GetOracleData(OracleConnection conn, string sql)
        //{
        //    OracleCommand cmd = conn.CreateCommand();

        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        cmd.CommandText = sql;
        //        OracleDataReader dataReader = cmd.ExecuteReader();
        //        dt.Load(dataReader);
        //        return new DataView(dt);
        //    }
        //    catch (Exception E)
        //    {
        //        throw E;
        //    }
        //    finally
        //    {
        //        if (cmd != null)
        //        {
        //            cmd.Dispose();
        //        }
        //    }
        //}

        public static DataBaseType ConvertToDataBaseType(string value)
        {
            if (value == "0")
            {
                return DataBaseType.Oracle;
            }
            else
            {
                return DataBaseType.SqlServer;
            }
        }
    }
}
