﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OracleClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;

namespace Genesis.Gtimes.EIS
{
    public class EISToStock : EISBase
    {

        #region 轉換欄位資訊

        /// <summary>
        /// 取得ERP來源資料
        /// </summary>
        /// <returns></returns>
        public DataView GetERPData(string[] Condition)
        {
            string ERPSQL = string.Empty;
           
            //if (Condition == null)
            //{
                // 第一次同步
                ERPSQL = @"
                SELECT STORAGE_NO PDNO,CREATE_USER,CREATE_DATE,OPERATION_NO,OPERATION_NAME,PART_NO,SORT_NO,
                QUANTITY,SENDQTY,UNIT,UNITRATE,ROUTETYPE,REMARK,WO,BATNO,LOT      
                FROM WP_MESTOWMS where STATE = 0  "; 
//            }
//            else
//            {
//                // 有上次查詢條件
//                string LastModifyDate = Condition[0].Trim();

//                ERPSQL = string.Format(@"
//                SELECT * FROM (
//                    SELECT STORAGE_NO PDNO,CREATE_USER,CREATE_DATE,OPERATION_NO,OPERATION_NAME,PART_NO,SORT_NO,
//                    QUANTITY,SENDQTY,UNIT,UNITRATE,ROUTETYPE,REMARK,WO,BATNO, Modified_Time as MODIFY_TIME      
//                    FROM WP_MESTOWMS where STATE = 0 
//                ) a 
//                WHERE MODIFY_TIME > '{0}'  
//                ORDER BY MODIFY_TIME
//                ", LastModifyDate);
//            }
                
            //DataView dvERP = dboERP.GetData(ERPSQL);
            DataView dvERP = dboMES.GetData(ERPSQL);
            return dvERP;
        }
         
        /// <summary>
        /// 新增出貨單資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int WriteStockData(DataBaseObject dbo, DataRow drvERP)
        {
            // 新增資料表名稱
            string tableName = "APS_APIPRODH";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);
             
            // 解析對應欄位 
            string pdno = drvERP["PDNO"].ToString().Trim();            //入库單號

            icb.InsertColumn("PDNO", pdno);
            icb.InsertColumn("DOBY", drvERP["CREATE_USER"].ToString().Trim());   //承办人
            icb.InsertColumn("DODT", Convert.ToDateTime(drvERP["CREATE_DATE"])); //承办日期
            DateTime dt = DateTime.Now;

            //將出貨單主檔放入集合內
            List<object> ins = new List<object>();
            ins.Add(icb.GetSqlCommand());

            //收集出貨單明細檔也放入集合內

            MESFunction mes = new MESFunction(dboERP);
            if (!mes.IsExistStockDetail(pdno, drvERP["SORT_NO"].ToString().Trim()))
            {
                WriteStockDetailsData(dbo,drvERP);
            }
            int iMaster = dbo.ExcuteCommand(ins);
            return iMaster; 
        }

        /// <summary>
        /// 更新出貨單資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int UpdateStockData(DataBaseObject dbo, DataRow drvERP, string pdno)
        {
            MESFunction mes = new MESFunction(dboERP);
            if (!mes.IsExistStockDetail(pdno, drvERP["SORT_NO"].ToString().Trim()))
            {
                WriteStockDetailsData(dbo, drvERP);
            }
            int iMaster =1;
            return iMaster;      
        }
        
        /// <summary>
        /// 新增出貨單明細檔-料號資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="ShipSID"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int WriteStockDetailsData(DataBaseObject dbo, DataRow drvERP)
        {
            // 新增資料表名稱
            string tableName = "APS_APIPRODI";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);

            DateTime dtUpdate = DateTime.Now;
            icb.InsertColumn("PDNO", drvERP["PDNO"].ToString().Trim());
            icb.InsertColumn("PARTNO", drvERP["OPERATION_NO"].ToString().Trim());
            icb.InsertColumn("PARTDESC", drvERP["OPERATION_NAME"].ToString().Trim());
            icb.InsertColumn("PPARTNO", drvERP["PART_NO"].ToString().Trim());
            icb.InsertColumn("SORTNO", drvERP["SORT_NO"].ToString().Trim());
            icb.InsertColumn("QTY", Convert.ToDecimal(drvERP["QUANTITY"]));
            icb.InsertColumn("SENDQTY", Convert.ToDecimal(drvERP["SENDQTY"]));
            icb.InsertColumn("UNIT", drvERP["UNIT"].ToString().Trim());
            icb.InsertColumn("UNITRATE", drvERP["UNITRATE"].ToString().Trim());
            icb.InsertColumn("ROUTETYPE", drvERP["ROUTETYPE"].ToString().Trim());
            icb.InsertColumn("REMARK", drvERP["REMARK"].ToString().Trim());
            icb.InsertColumn("OREFNO", drvERP["WO"].ToString().Trim());
            icb.InsertColumn("BATNO", drvERP["BATNO"].ToString().Trim());
            icb.InsertColumn("LOT", drvERP["LOT"].ToString().Trim());
            //icb.InsertColumn("CREATE_USER", WriteUserID);
            //icb.InsertColumn("CREATE_DATE", dtUpdate);
            //icb.InsertColumn("UPDATE_USER", WriteUserID);
            //icb.InsertColumn("UPDATE_DATE", dtUpdate);

            int iMaster = dbo.ExcuteCommand(icb.GetSqlCommand());
            return iMaster;           
        }

        /// <summary>
        /// 更新ERP處理狀態
        /// </summary>
        public void UpdateERPState(DataBaseObject dbo, string ShipNo, string State)
        {
            // 異動資料表名稱
            string tableName = "WP_MESTOWMS";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("STORAGE_NO", ShipNo);
         
            ucb.UpdateColumn("STATE", State);

            dbo.ExcuteCommand(ucb.GetSqlCommand());
        }

        #endregion 
        
        private string WriteAlarmJob(DataBaseObject dbo)
        {          
            string SendSubject = "[EIS]轉檔錯誤通知--入库單";
            string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;
            return MESFunction.WriteAlarmJob(dbo, SendSubject, SendFilePath);
        }
                 
        public void Start(string[] args)
        {            
            nlog = LogManager.GetLogger(this.GetType().Name);

            DateTime Job_StartTime = DateTime.Now;

            //Keep Process SID, 檢測每次轉檔執行緒
            string EIS_SID = "Stock";
            LogAction LogAction;

            // 記錄是否發送Email
            bool AlarmJobFlag = false;

            DateTime EISOldLastUpdate = DateTime.Now;

            try
            {                
                if (args.Length > 0)
                {
                    EIS_SID = args[0].Trim();
                }

                // 連線資料庫
                ConnectDataBase();

                // 記錄Log : 開始轉出貨單
                TraceNormalLog("Start Sync !");

                #region 判斷是否有multiProcess狀況

                bool multiProcess = false;
                string[] Condition = null;

                //取出EIS_JOB執行狀況,回傳至EISResult  
                dboEIS.BeginTransaction();
                EISResult EIS = EISFunction.CheckEISResult(dboEIS, EIS_SID, Condition, EISOldLastUpdate);
                dboEIS.Commit();

                multiProcess = EIS.multiProcess;
                Condition = EIS.Condition;
                EISOldLastUpdate = EIS.EISOldLastUpdate;

                if (multiProcess)
                {
                    // 當 JOB 狀態為 Run, 表示有平行處理的問題 
                    // 新增紀錄Log (EIS_JOBHIST), 不更新JOB CONTROL 
                    dboEIS.BeginTransaction();
                    int iLogCount =
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[STOCK] Job State=Run, Muti Process!!");
                    dboEIS.Commit();

                    TraceNormalLog("JOB 狀態為 Run ,可能有其他相同程式也在執行");
                     
                    return;
                }

                #endregion

                #region Get ERP Data

                //取得ERP來源資料
                DataView dvERP = GetERPData(Condition);

                #endregion
                 
                #region Parser ERP Data

                // 記錄Log : 開始解析ERP來源出貨單 
                TraceNormalLog("Start Parser : " + dvERP.Count.ToString());

                string NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                string LogProcessDesc = string.Empty;
                
                if (Condition == null)
                {
                    //第一次同步
                    LogProcessDesc = "First Sync";
                    LogAction = LogAction.Insert;
                }
                else
                {
                    //有上次查詢條件
                    LogProcessDesc = "Data Sync";
                    LogAction = LogAction.DataSync;
                }

                int iRowCount = 0;
                int iInsertCount = 0;
                int iUpdateCount = 0;

                if (dvERP != null && dvERP.Count > 0)   
                {
                    #region 有異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=Idle)

                    MESFunction mes = new MESFunction(dboERP);
                    int iUpdSuccess = 0;
                    int iInsSuccess = 0;
                    string spdno = string.Empty; 

                    dboMES.BeginTransaction();

                    foreach (DataRow row in dvERP.Table.Rows)
                    {                     
                        iUpdSuccess = 0;
                        iInsSuccess = 0;
                        spdno = row["PDNO"].ToString().Trim(); 

                        //檢查是否已有
                        if (mes.IsExistStock(spdno))
                        {
                            try
                            {
                                //回傳UPDATE成功筆數 
                                iUpdSuccess = UpdateStockData(dboERP, row, spdno); 
                                if (iUpdSuccess > 0)
                                {
                                    iUpdateCount += 1;
                                    //Console.WriteLine(" U " + iUpdateCount.ToString() + ".SO :" + sSO);
                                    UpdateERPState(dboMES, spdno, "1");
                                }
                            }
                            catch (Exception ex)
                            {                               
                                // 記錄Log
                                TraceNormalLog("UPDATE|" + spdno + " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;

                                UpdateERPState(dboMES, spdno, "2");
                            }
                        }
                        else
                        {
                            try
                            {
                                //回傳INSERT成功筆數 
                                iInsSuccess = WriteStockData(dboERP, row);
                                if (iInsSuccess > 0)
                                {
                                    iInsertCount += 1;
                                    //Console.WriteLine(" I " + iInsertCount.ToString() + ".SO :" + sSO);
                                    UpdateERPState(dboMES, spdno, "1");
                                }
                            }
                            catch (Exception ex)
                            {
                                // 記錄Log 
                                TraceNormalLog("INSERT|" + spdno +  " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;

                                UpdateERPState(dboMES, spdno, "2");
                            }
                        }
                    }
                
                    iRowCount = iUpdateCount + iInsertCount;
                    LogProcessDesc += ": Check Row=" + dvERP.Count + ", Update Row=" + iUpdateCount + ", Insert Row=" + iInsertCount;
                    //NextUpdateDate = dvERP[dvERP.Count - 1]["MODIFY_TIME"].ToString();
                    //if (NextUpdateDate.Equals(""))
                        NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                     
                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, iRowCount, "[STOCK] " + LogProcessDesc);
                    int EIS_JobControlCount = 0;

                    if (AlarmJobFlag == false)
                    {
                        // *** 重要 *** 
                        // 更新 EIS_JOBCONTROL,記錄下次轉檔的比對時間(NextUpdateDate) 
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, NextUpdateDate, EISOldLastUpdate);
                    }
                    else
                    { 
                        //更新 EIS_JOBCONTROL, 切換State回Idle  
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                    }

                    if (EIS_LogCount > 0 && EIS_JobControlCount > 0)
                    {
                        try
                        {
                            dboEIS.Commit();
                            dboMES.Commit();
                            dboERP.Commit();
                        }
                        catch (Exception ex)
                        {
                            TraceErrorLog(ex);
                            dboMES.Rollback();
                            dboERP.Rollback();
                        }
                    }
                    else
                    {
                        dboEIS.Commit();
                        dboMES.Rollback();
                        dboERP.Rollback();
                    }

                    #endregion
                }
                else
                {
                    #region 無異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    LogProcessDesc += ": 無異動紀錄";

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, 0, "[STOCK] " + LogProcessDesc);

                    //更新 EIS_JOBCONTROL, 並記錄下次轉檔的比對時間 
                    int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, Condition[0].Trim(), EISOldLastUpdate);

                    dboEIS.Commit();

                    #endregion
                }

                #endregion
                 
                Console.WriteLine("End Parser : " + LogProcessDesc);

                // 記錄Log : 結束解析ERP來源出貨單 
                TraceNormalLog("End Parser : " + LogProcessDesc);
                TraceNormalLog("End Sync !");
                TraceNormalLog("-------------------------------------------------------");

                #region 寫入Alarm發送EMail
                if (AlarmJobFlag)
                {                   
                    string AlarmMsg = WriteAlarmJob(dboMES);
                    if (!AlarmMsg.Equals("Success"))
                    {
                        dboEIS.BeginTransaction();
                        //新增Log (EIS_JOBHIST)
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Alarm] 有錯誤發生, ex:" + AlarmMsg);
                        dboEIS.Commit();
                    }
                }
                #endregion 
            }
            catch (Exception e)
            {
                TraceErrorLog(e);

                dboEIS.BeginTransaction();
                //更新 EIS_JOBCONTROL, 切換State回Idle  
                int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                dboEIS.Commit();
            }
            finally
            {
                if (dboEIS != null)
                {
                    dboEIS.ColseConnect();
                }
                if (dboERP != null)
                {
                    dboERP.ColseConnect();
                }
                if (dboMES != null)
                {
                    dboMES.ColseConnect();
                }
            }
        }
         
    }
}
