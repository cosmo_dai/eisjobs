﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Configuration;
using Genesis.Gtimes.Common;

namespace EqpHourlySummary
{
    public class Program
    {
        static int Main(string[] args)
        {
            bool isEisServiceCall = Genesis.EisJobCommon.EisJobBase.IsEisServiceCall();

            ////本機測試, 須指定EIS_JOBCONTROL.EIS_SID
            string[] test = { "EqpStatus" };
            args = test;

            if (args.Length == 0)
            {
                Console.WriteLine("未傳入EIS_SID作為第一個參數.");
                //if (!isEisServiceCall)
                //    Console.ReadKey();
                    return 1;
            }

            using (DBController eisDbc = new DBController(ConfigurationManager.ConnectionStrings["EIS_DB"]))
            {
                EqpHourlySummary utl = new EqpHourlySummary(eisDbc, args);
                Stopwatch watch = new Stopwatch();
                watch.Start();
                try
                {
                    utl.DoJob();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    //if (!isEisServiceCall)
                    //    Console.ReadKey();
                    return 2;
                }
                watch.Stop();
                Console.WriteLine("本次WIP每5分钟進出量統計花費時間: {0} 秒", watch.Elapsed.TotalSeconds);
            }
            //if (!isEisServiceCall)
            //    Console.ReadKey();

            return 0;
        }
    }
}
