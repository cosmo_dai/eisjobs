﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Configuration;
using Genesis.Gtimes.Common;
using System.Data.SqlClient;
using System.Data.OracleClient;

namespace EqpHourlySummary
{
    public class EqpHourlySummary : Genesis.EisJobCommon.EisJobBase
    {
        System.Configuration.ConnectionStringSettingsCollection config = System.Configuration.ConfigurationManager.ConnectionStrings;
        public EqpHourlySummary(DBController eisDbc, string[] args) : base(eisDbc, args) { }
        public DataTable EqpStateDT;

        protected override void DoCustomJob()
        {
            //ControlValue = "2014-11-03 21:00:00;3600";

            #region "----- 基本處理-----"

            //上次的統計時間 
            DateTime? lastSyncTime = null;

            //本次統計的總秒數, 彈性留給欲轉某固定時段的統計資料
            int Controlinterval = 0;

            // 抓取EIS控制資料 也就是下次要抓取資料的時間區間開始值
            if (!string.IsNullOrEmpty(ControlValue))
            {
                string[] ControlValueString = ControlValue.ToString().Split(';');

                lastSyncTime = DateTime.ParseExact(ControlValueString[0], "yyyy-MM-dd HH:mm:ss", null);
                if (ControlValueString.Length > 1) Controlinterval = System.Convert.ToInt32(ControlValueString[1]);
            }
            else
            {
                throw new Exception("要求設置JOB的控制變量,格式為 yyyy-MM-dd HH:mm:ss;3600(秒)");
            }

            #endregion

            //本次處理開始時間
            DateTime tmpStartTime = System.Convert.ToDateTime(lastSyncTime);

            //本次統計的結束時間
            DateTime thisSyncEndTime = this.DBC.GetDBTime();

            //若為3600秒，表示由設定值起，只轉某固定一小時的統計資料，7200秒則以此類推...
            //若為0，表示要轉至現在時間為止，由設定值起直至現在時間，每小時統計資料。
            if (Controlinterval > 0)
            {
                thisSyncEndTime = tmpStartTime.AddSeconds(Controlinterval);   //加入本次要轉的總秒數, 預設是3600秒
            }
            else if (Controlinterval == 0)
            {
                thisSyncEndTime = new DateTime(thisSyncEndTime.Year, thisSyncEndTime.Month, thisSyncEndTime.Day, thisSyncEndTime.Hour, 0, 0); // 忽略分,秒
            }

            //本次統計的結束時間
            DateTime tmpEndTime = thisSyncEndTime;
            
            if (tmpStartTime >= tmpEndTime)
            {
                this.LogSkip("尚不能進行新一輪資料統計,最後統計時間為: {0:yyyy-MM-dd HH:mm:ss}", lastSyncTime.Value);
            }
            else
            {
                SetJobAction("開始統計機台本次變更狀態的LOG");

                #region "----- 機台資料 -----"

                Stopwatch timer = new Stopwatch();
                DBController EIS_FROM = new DBController(config["EIS_FROM"]);
                DBController EIS_TO = new DBController(config["EIS_TO"]);
                List<IDbCommand> InsertDBCmd = new List<IDbCommand>();

                //抓取機台的清單
                string GetEqpList = @"
                    SELECT A.EQP_SID, A.EQP_NO, A.EQP_NAME, A.PROPERTY_SN, A.EQP_DEVICE_NO, A.EQP_SEQ, A.LOCATION, A.ENABLE_FLAG, A.STATE_SID, A.STATE_NO, 
                        A.TYPE_SID, A.TYPE_NO, A.OWNER, A.VENDOR, A.LAST_CHANGE_STATE_TIME, B.STATE_NAME, B.AVAILTIME_FLAG, B.LOSTTIME_FLAG
                    FROM FC_EQUIPMENT A INNER JOIN FC_EQUIPMENT_STATE B ON A.STATE_SID = B.STATE_SID                                                                                  
                    ";

                DataTable EqpListDT = EIS_FROM.Select(GetEqpList);

                #endregion

                #region 依機台看每一小時段的記錄
                for (int i = 0; i < EqpListDT.Rows.Count; i++)
                {
                    string eqpSID = EqpListDT.Rows[i]["EQP_SID"].ToString().Trim();

                    DateTime _startTime;        //目前處理之資料起始時間
                    DateTime _endTime = default(DateTime);          //目前處理之資料結束時間
                    DateTime flagStartTime;     //以小時間隔之起始時間
                    DateTime flagEndTime;       //以小時間隔之結束時間
                    int stateTime;

                    #region "--- get trace data by equpment ---"

                    string GetEqpTrace = @"
                        SELECT DISTINCT A.EQP_SID, A.EQP_NO, C.STATE_SID, B.OLD_STATE_NO STATE_NO, C.STATE_NAME, B.LAST_CHANGE_STATE_TIME START_TIME, B.CREATE_DATE END_TIME , C.AVAILTIME_FLAG, C.LOSTTIME_FLAG
                        FROM FC_EQUIPMENT A INNER JOIN WP_EQP_TRACE B ON A.EQP_SID = B.EQP_SID INNER JOIN FC_EQUIPMENT_STATE C ON B.OLD_STATE_NO = C.STATE_NO
                        WHERE (A.EQP_SID = :EQPSID) AND B.CREATE_DATE>=:PERIOD_BEGINTIME AND B.LAST_CHANGE_STATE_TIME <:PERIOD_ENDTIME 
                        AND B.NEW_STATE_NO<>B.OLD_STATE_NO 
                        ORDER by B.CREATE_DATE, B.LAST_CHANGE_STATE_TIME
                        ";
                    IDbCommand GetEqpTracecmd = EIS_FROM.CreateCommand();
                    GetEqpTracecmd.CommandText = EIS_FROM.GetCommandText(GetEqpTrace, SQLStringType.OracleSQLString);

                    IDbDataParameter paramBeginTime = GetEqpTracecmd.CreateParameter();
                    IDbDataParameter paramEndTime = GetEqpTracecmd.CreateParameter();
                    IDbDataParameter paramEqpSid = GetEqpTracecmd.CreateParameter();
                    paramBeginTime.ParameterName = "PERIOD_BEGINTIME";
                    paramBeginTime.DbType = DbType.Date;
                    paramEndTime.ParameterName = "PERIOD_ENDTIME";
                    paramEndTime.DbType = DbType.Date;
                    paramEqpSid.ParameterName = "EQPSID";
                    paramEqpSid.DbType = DbType.String;
                    GetEqpTracecmd.Parameters.Add(paramBeginTime);
                    GetEqpTracecmd.Parameters.Add(paramEndTime);
                    GetEqpTracecmd.Parameters.Add(paramEqpSid);
                    paramBeginTime.Value = tmpStartTime;
                    paramEndTime.Value = tmpEndTime;
                    paramEqpSid.Value = eqpSID;
                    RowCount = 0;
                    //string EndState = "";
                    //DateTime EndUpdate = tmpEndTime;
                    
                   

                    #endregion

                    if (DBC.ProviderName.Contains("Oracle"))
                    {
                        OracleDataReader EqpTrace = (OracleDataReader)GetEqpTracecmd.ExecuteReader();

                        #region "-----機台狀態有變更之資料處理-----"

                        if (EqpTrace.HasRows)
                        {
                            DateTime flagTime = default(DateTime);    //因為弘凱的WP_EQP_TRACE的last_change_state_time會有重複，所以紀錄上一筆的create time為下一筆的last change state time
                            while (EqpTrace.Read())
                            {
                                if (default(DateTime).Equals(flagTime))
                                    _startTime = System.Convert.ToDateTime(EqpTrace["START_TIME"].ToString().Trim());
                                else
                                    _startTime = flagTime;

                                _endTime = System.Convert.ToDateTime(EqpTrace["END_TIME"].ToString().Trim());

                                //flag start time
                                if (_startTime < tmpStartTime) flagStartTime = tmpStartTime; else flagStartTime = _startTime;

                                //flag end time
                                flagEndTime = System.Convert.ToDateTime(flagStartTime.AddHours(1).ToString("yyyy-MM-dd HH:00:00"));
                                if (flagEndTime > tmpEndTime) flagEndTime = tmpEndTime;
                                if (flagEndTime > _endTime) flagEndTime = _endTime;

                                stateTime = 0;
                                while (flagStartTime < _endTime)
                                {
                                    stateTime = (int)(flagEndTime - flagStartTime).TotalSeconds;
                                    if (stateTime > 0)
                                    {
                                        #region "----- insert a new row -----"
                                        InsertDBCmd.Add(
                                            DoInsertEqpHourlySummary(
                                                EIS_TO,
                                                flagStartTime.Year.ToString(),
                                                flagStartTime.Month.ToString(),
                                                flagStartTime.Day.ToString(),
                                                flagStartTime.Hour.ToString(),
                                                EqpListDT.Rows[i]["EQP_SID"].ToString().Trim(),
                                                EqpListDT.Rows[i]["EQP_NO"].ToString().Trim(),
                                                EqpListDT.Rows[i]["EQP_NAME"].ToString().Trim(),
                                                EqpListDT.Rows[i]["PROPERTY_SN"].ToString().Trim(),
                                                EqpListDT.Rows[i]["EQP_DEVICE_NO"].ToString().Trim(),
                                                EqpListDT.Rows[i]["LOCATION"].ToString().Trim(),
                                                EqpListDT.Rows[i]["ENABLE_FLAG"].ToString().Trim(),
                                                EqpListDT.Rows[i]["TYPE_SID"].ToString().Trim(),
                                                EqpListDT.Rows[i]["TYPE_NO"].ToString().Trim(),
                                                EqpListDT.Rows[i]["OWNER"].ToString().Trim(),
                                                EqpListDT.Rows[i]["VENDOR"].ToString().Trim(),
                                                EqpTrace["STATE_SID"].ToString().Trim(),
                                                EqpTrace["STATE_NO"].ToString().Trim(),
                                                EqpTrace["STATE_NAME"].ToString().Trim(),
                                                stateTime,
                                                EqpTrace["AVAILTIME_FLAG"].ToString().Trim(),
                                                EqpTrace["LOSTTIME_FLAG"].ToString().Trim(),
                                                flagStartTime,
                                                flagEndTime));
                                        #endregion
                                        RowCount++;
                                    }
                                    flagTime = flagEndTime;
                                    flagStartTime = flagEndTime;
                                    flagEndTime = flagEndTime.AddHours(1);
                                    if (flagEndTime > tmpEndTime) flagEndTime = tmpEndTime;
                                    if (flagEndTime > _endTime) flagEndTime = _endTime;
                                    if (flagStartTime == flagEndTime) break;
                                }
                            }

                            // 如果不是Oracle资料库,则暂停0.3秒后执行,以免生成的SID重复
                            if (!EIS_TO.ProviderName.Contains("Oracle")) System.Threading.Thread.Sleep(300);
                        }
                        #endregion

                        EqpTrace.Close();
                        EqpTrace.Dispose();
                    }
                    else
                    {
                        SqlDataReader EqpTrace = (SqlDataReader)GetEqpTracecmd.ExecuteReader();
                        #region "-----機台狀態有變更之資料處理-----"

                        if (EqpTrace.HasRows)
                        {
                            DateTime flagTime = default(DateTime);    //因為弘凱的WP_EQP_TRACE的last_change_state_time會有重複，所以紀錄上一筆的create time為下一筆的last change state time
                            while (EqpTrace.Read())
                            {
                                if (default(DateTime).Equals(flagTime))
                                    _startTime = System.Convert.ToDateTime(EqpTrace["START_TIME"].ToString().Trim());
                                else
                                    _startTime = flagTime;

                                _endTime = System.Convert.ToDateTime(EqpTrace["END_TIME"].ToString().Trim());

                                //flag start time
                                if (_startTime < tmpStartTime) flagStartTime = tmpStartTime; else flagStartTime = _startTime;

                                //flag end time
                                flagEndTime = System.Convert.ToDateTime(flagStartTime.AddHours(1).ToString("yyyy-MM-dd HH:00:00"));
                                if (flagEndTime > tmpEndTime) flagEndTime = tmpEndTime;
                                if (flagEndTime > _endTime) flagEndTime = _endTime;

                                stateTime = 0;
                                while (flagStartTime < _endTime)
                                {
                                    stateTime = (int)(flagEndTime - flagStartTime).TotalSeconds;
                                    if (stateTime > 0)
                                    {
                                        #region "----- insert a new row -----"
                                        InsertDBCmd.Add(
                                            DoInsertEqpHourlySummary(
                                                EIS_TO,
                                                flagStartTime.Year.ToString(),
                                                flagStartTime.Month.ToString(),
                                                flagStartTime.Day.ToString(),
                                                flagStartTime.Hour.ToString(),
                                                EqpListDT.Rows[i]["EQP_SID"].ToString().Trim(),
                                                EqpListDT.Rows[i]["EQP_NO"].ToString().Trim(),
                                                EqpListDT.Rows[i]["EQP_NAME"].ToString().Trim(),
                                                EqpListDT.Rows[i]["PROPERTY_SN"].ToString().Trim(),
                                                EqpListDT.Rows[i]["EQP_DEVICE_NO"].ToString().Trim(),
                                                EqpListDT.Rows[i]["LOCATION"].ToString().Trim(),
                                                EqpListDT.Rows[i]["ENABLE_FLAG"].ToString().Trim(),
                                                EqpListDT.Rows[i]["TYPE_SID"].ToString().Trim(),
                                                EqpListDT.Rows[i]["TYPE_NO"].ToString().Trim(),
                                                EqpListDT.Rows[i]["OWNER"].ToString().Trim(),
                                                EqpListDT.Rows[i]["VENDOR"].ToString().Trim(),
                                                EqpTrace["STATE_SID"].ToString().Trim(),
                                                EqpTrace["STATE_NO"].ToString().Trim(),
                                                EqpTrace["STATE_NAME"].ToString().Trim(),
                                                stateTime,
                                                EqpTrace["AVAILTIME_FLAG"].ToString().Trim(),
                                                EqpTrace["LOSTTIME_FLAG"].ToString().Trim(),
                                                flagStartTime,
                                                flagEndTime));
                                        #endregion
                                        RowCount++;
                                    }
                                    flagTime = flagEndTime;
                                    flagStartTime = flagEndTime;
                                    flagEndTime = flagEndTime.AddHours(1);
                                    if (flagEndTime > tmpEndTime) flagEndTime = tmpEndTime;
                                    if (flagEndTime > _endTime) flagEndTime = _endTime;
                                    if (flagStartTime == flagEndTime) break;
                                }
                            }

                            // 如果不是Oracle资料库,则暂停0.3秒后执行,以免生成的SID重复
                            if (!EIS_TO.ProviderName.Contains("Oracle")) System.Threading.Thread.Sleep(300);
                        }
                        #endregion

                        EqpTrace.Close();
                        EqpTrace.Dispose();
                    }

                    #region "-----不論有無機台狀態變更，自機台基本資料轉入最後狀態變更至目前的稼動-----"

                    DataRow[] drs = EqpListDT.Select(
                        string.Format("EQP_SID='{0}' AND ('{1}'>=LAST_CHANGE_STATE_TIME OR '{2}'>=LAST_CHANGE_STATE_TIME)",
                        eqpSID, tmpStartTime.ToString("yyyy-MM-dd HH:mm:ss"), tmpEndTime.ToString("yyyy-MM-dd HH:mm:ss")));

                    if (drs != null && drs.Length > 0)
                    {
                        foreach (DataRow dr in drs)
                        {
                            _startTime = System.Convert.ToDateTime(dr["LAST_CHANGE_STATE_TIME"].ToString().Trim());

                            //若機台狀態有變更資料, 則需比較最後一筆的LAST_CHANGE_STATE_TIME
                            //避免機台基本資料跟歷史檔的最後狀態變更有時間差                        
                            if (_endTime != null)
                            {
                                if (_endTime < _startTime)
                                    _startTime = _endTime;
                            }

                            _endTime = tmpEndTime;

                            //flag start time
                            if (_startTime < tmpStartTime) flagStartTime = tmpStartTime; else flagStartTime = _startTime;

                            //flag end time
                            flagEndTime = System.Convert.ToDateTime(flagStartTime.AddHours(1).ToString("yyyy-MM-dd HH:00:00"));
                            if (flagEndTime > tmpEndTime) flagEndTime = tmpEndTime;

                            if (flagEndTime > _endTime) flagEndTime = _endTime;

                            stateTime = 0;
                            while (flagStartTime < _endTime)
                            {
                                stateTime = (int)(flagEndTime - flagStartTime).TotalSeconds;
                                if (stateTime > 0)
                                {
                                    #region 檢查最後一筆是否有重複筆數

                                    bool ExistFlag = false;
                                    for (int j = InsertDBCmd.Count - 1; j >= 0; j--)
                                    {
                                        IDbCommand item = InsertDBCmd[j];

                                        IDbDataParameter paramYear1 = (IDbDataParameter)item.Parameters["Year"];
                                        IDbDataParameter paramMonth1 = (IDbDataParameter)item.Parameters["Month"];
                                        IDbDataParameter paramDay1 = (IDbDataParameter)item.Parameters["Day"];
                                        IDbDataParameter paramHour1 = (IDbDataParameter)item.Parameters["Hour"];
                                        IDbDataParameter paramEqpSid1 = (IDbDataParameter)item.Parameters["EQP_SID"];
                                        IDbDataParameter paramTypeSid1 = (IDbDataParameter)item.Parameters["TYPE_SID"];
                                        IDbDataParameter paramStateSid1 = (IDbDataParameter)item.Parameters["STATE_SID"];
                                        IDbDataParameter paramStateTime1 = (IDbDataParameter)item.Parameters["STATE_TIME"];
                                        IDbDataParameter paramStartTime1 = (IDbDataParameter)item.Parameters["STARTTIME"];
                                        IDbDataParameter paramEndTime1 = (IDbDataParameter)item.Parameters["ENDTIME"];

                                        if ((Convert.ToInt32(paramYear1.Value) == flagStartTime.Year) &&
                                            (Convert.ToInt32(paramMonth1.Value) == flagStartTime.Month) &&
                                            (Convert.ToInt32(paramDay1.Value) == flagStartTime.Day) &&
                                            (Convert.ToInt32(paramHour1.Value) == flagStartTime.Hour) &&
                                            (Convert.ToString(paramEqpSid1.Value) == dr["EQP_SID"].ToString().Trim()) &&
                                            (Convert.ToString(paramTypeSid1.Value) == dr["TYPE_SID"].ToString().Trim()) &&
                                            (Convert.ToString(paramStateSid1.Value) == dr["STATE_SID"].ToString().Trim()) &&
                                            (Convert.ToInt32(paramStateTime1.Value) == stateTime) &&
                                            (Convert.ToDateTime(paramStartTime1.Value) == flagStartTime) &&
                                            (Convert.ToDateTime(paramEndTime1.Value) == flagEndTime)
                                            )
                                        {
                                            ExistFlag = true;
                                            break;
                                        }
                                    }
                                    #endregion

                                    if (ExistFlag == false)
                                    {
                                        #region "----- insert a new row -----"
                                        InsertDBCmd.Add(
                                            DoInsertEqpHourlySummary(
                                                EIS_TO,
                                                flagStartTime.Year.ToString(),
                                                flagStartTime.Month.ToString(),
                                                flagStartTime.Day.ToString(),
                                                flagStartTime.Hour.ToString(),
                                                dr["EQP_SID"].ToString().Trim(),
                                                dr["EQP_NO"].ToString().Trim(),
                                                dr["EQP_NAME"].ToString().Trim(),
                                                dr["PROPERTY_SN"].ToString().Trim(),
                                                dr["EQP_DEVICE_NO"].ToString().Trim(),
                                                dr["LOCATION"].ToString().Trim(),
                                                dr["ENABLE_FLAG"].ToString().Trim(),
                                                dr["TYPE_SID"].ToString().Trim(),
                                                dr["TYPE_NO"].ToString().Trim(),
                                                dr["OWNER"].ToString().Trim(),
                                                dr["VENDOR"].ToString().Trim(),
                                                dr["STATE_SID"].ToString().Trim(),
                                                dr["STATE_NO"].ToString().Trim(),
                                                dr["STATE_NAME"].ToString().Trim(),
                                                stateTime,
                                                dr["AVAILTIME_FLAG"].ToString().Trim(),
                                                dr["LOSTTIME_FLAG"].ToString().Trim(),
                                                flagStartTime,
                                                flagEndTime));
                                        #endregion
                                        RowCount++;
                                    }

                                }
                                flagStartTime = flagEndTime;
                                flagEndTime = flagStartTime.AddHours(1);
                                if (flagEndTime > tmpEndTime) flagEndTime = tmpEndTime;
                                if (flagEndTime > _endTime) flagEndTime = _endTime;
                                if (flagStartTime == flagEndTime) break;
                            }
                        }

                        // 如果不是Oracle资料库,则暂停0.3秒后执行,以免生成的SID重复
                        if (!EIS_TO.ProviderName.Contains("Oracle")) System.Threading.Thread.Sleep(300);
                    }
                    #endregion

                }
                #endregion

                #region 刪除統計時段已有資料的SQL
                // 這樣將允許重新運行JOB以獲得統計資料.            
                string sqlDelete = @"DELETE FROM WR_EQPHOURLYSTATESUMMARY WHERE STARTTIME >= :PERIOD_BEGINTIME and STARTTIME < :PERIOD_ENDTIME";
                IDbCommand cmdDelete = EIS_TO.CreateCommand();
                cmdDelete.CommandText = EIS_TO.GetCommandText(sqlDelete, SQLStringType.OracleSQLString);

                IDbDataParameter paramDeleteBeginTime = cmdDelete.CreateParameter();
                paramDeleteBeginTime.ParameterName = "PERIOD_BEGINTIME";
                paramDeleteBeginTime.DbType = DbType.Date;
                cmdDelete.Parameters.Add(paramDeleteBeginTime);

                IDbDataParameter paramDeleteEndTime = cmdDelete.CreateParameter();
                paramDeleteEndTime.ParameterName = "PERIOD_ENDTIME";
                paramDeleteEndTime.DbType = DbType.Date;
                cmdDelete.Parameters.Add(paramDeleteEndTime);

                #region 固定統計時間區段為 1 小時
                DateTime tmpDeleteLoopStartTime = tmpStartTime;
                DateTime tmpDeleteLoopEndTime = tmpStartTime.AddHours(1);
                DateTime tmpDeleteEndTime = tmpEndTime;
                #endregion

                #region 對能夠統計的時間,以小時為區間,進行分段循環執行刪除
                int iCountDeleted = 0;
                while (tmpDeleteLoopEndTime <= tmpDeleteEndTime)
                {
                    paramDeleteBeginTime.Value = tmpDeleteLoopStartTime;
                    paramDeleteEndTime.Value = tmpDeleteLoopEndTime;

                    // 啟動事務, 刪除某個小時段的資料
                    using (IDbTransaction tx = EIS_TO.GetTransaction())
                    {
                        //刪除已有統計資料
                        cmdDelete.Transaction = tx;
                        iCountDeleted = cmdDelete.ExecuteNonQuery();
                        tx.Commit();
                    }

                    //Log寫入EIS_JOBHIST
                    this.LogDebug("刪除結束時間點 {0:yyyy-MM-dd HH:mm:ss}，刪除 {1} 筆舊資料", tmpDeleteLoopStartTime, iCountDeleted);

                    tmpDeleteLoopStartTime = tmpDeleteLoopStartTime.AddHours(1);
                    tmpDeleteLoopEndTime = tmpDeleteLoopEndTime.AddHours(1);
                }
                #endregion

                #endregion

                // 啟動事務, 統計某個小時段成功的資料
                RowCount = EIS_TO.DoTransaction(InsertDBCmd);

                // 啟動事務, 以便某個小時段統計成功的資料和CONTROL_VALUE保持一致.
                //IDbCommand cmdInsert = this.DBC.CreateCommand();
                using (IDbTransaction tx = this.DBC.GetTransaction())
                {
                    //統計資料并插入到統計資料表中                
                    //cmdInsert.Transaction = tx; 

                    //回寫Control_Value
                    SaveControlValue(this.DBC, tx, tmpEndTime.ToString("yyyy-MM-dd HH:mm:ss") + ";" + Controlinterval.ToString());
                    tx.Commit();
                }

                this.LogFinish("本次JOB運行成功統計到共 {0} 筆資料.", RowCount);
            }
        }

        public IDbCommand DoInsertEqpHourlySummary(DBController MyDBC, string YEAR, string MONTH, string DAY, string HOUR, string EQP_SID, string EQP_NO, string EQP_NAME, string PROPERTY_SN, string EQP_DEVICE_NO, string LOCATION, string EQP_ENABLE_FLAG, string TYPE_SID, string TYPE_NO, string OWNER, string VENDOR, string STATE_SID, string STATE_NO, string STATE_NAME, Int32 STATE_TIME, string AVAILTIME_FLAG, string LOSTTIME_FLAG, DateTime STARTTIME, DateTime ENDTIME)
        {
            InsertCommandBuilder insert = new InsertCommandBuilder(MyDBC, "WR_EQPHOURLYSTATESUMMARY");
            insert.InsertColumn("SID", MyDBC.GetSID());
            insert.InsertColumn("YEAR", YEAR);
            insert.InsertColumn("MONTH", MONTH);
            insert.InsertColumn("DAY", DAY);
            insert.InsertColumn("HOUR", HOUR);
            insert.InsertColumn("EQP_SID", EQP_SID);
            insert.InsertColumn("EQP_NO", EQP_NO);
            insert.InsertColumn("EQP_NAME", EQP_NAME);
            insert.InsertColumn("PROPERTY_SN", PROPERTY_SN);
            insert.InsertColumn("EQP_DEVICE_NO", EQP_DEVICE_NO);
            insert.InsertColumn("LOCATION", LOCATION);
            insert.InsertColumn("EQP_ENABLE_FLAG", EQP_ENABLE_FLAG);
            insert.InsertColumn("TYPE_SID", TYPE_SID);
            insert.InsertColumn("TYPE_NO", TYPE_NO);
            insert.InsertColumn("OWNER", OWNER);
            insert.InsertColumn("VENDOR", VENDOR);
            insert.InsertColumn("STATE_SID", STATE_SID);
            insert.InsertColumn("STATE_NO", STATE_NO);
            insert.InsertColumn("STATE_NAME", STATE_NAME);
            insert.InsertColumn("STATE_TIME", STATE_TIME);
            insert.InsertColumn("AVAILTIME_FLAG", AVAILTIME_FLAG);
            insert.InsertColumn("LOSTTIME_FLAG", LOSTTIME_FLAG);
            insert.InsertColumn("STARTTIME", STARTTIME);
            insert.InsertColumn("ENDTIME", ENDTIME);
            return insert.GetCommand();
        }

    }
}
