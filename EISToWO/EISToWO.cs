﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OracleClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;

namespace Genesis.Gtimes.EIS
{
    public class EISToWO : EISBase
    { 
        #region 轉換欄位資訊

        /// <summary>
        /// 取得ERP來源資料
        /// </summary>
        /// <returns></returns>
        public DataView GetERPData(string[] Condition)
        {
            string ERPSQL = string.Empty;

            //if (Condition == null)
            //{
                // 第一次同步
                // ZSTATUS 狀態 (空值為新工單, 有值為結單)
                // MANDT='888' 是指正式區
                ERPSQL = @"Select   WO,WO_TYPE,QUANTITY,UNIT,QTY_1,UNIT_1,QTY_2,UNIT_2,ROUTE,PARTNO,LAYOUT,DEPT,SO,CREATER                        
                From MES_WO where STATE = 0 ";                 
//            } 
//            else
//            {
//                // 有上次查詢條件
//                string LastModifyDate = Condition[0].Trim();

//                ERPSQL = string.Format(@"
//                   SELECT * FROM (
//                    Select  WO,WO_TYPE,QUANTITY,UNIT,QTY_1,UNIT_1,QTY_2,UNIT_2,ROUTE,PARTNO 
//                            , Modified_Time as MODIFY_TIME      
//                    From MES_WO where STATE = 0 
//                ) a 
//                WHERE MODIFY_TIME > '{0}' 
//                ORDER BY MODIFY_TIME
//                ", LastModifyDate);
//            }
            DataView dvERP = dboERP.GetData(ERPSQL);
            return dvERP;
        }
         
        /// <summary>
        /// 新增工單資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int WriteWOData(DataBaseObject dbo, DataRowView drvERP)
        {
            // 新增資料表名稱
            string tableName = "WP_WO";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);

            // 解析對應欄位               
            string WO = drvERP["WO"].ToString().Trim();
            string WO_TYPE = drvERP["WO_TYPE"].ToString().Trim();
            string PartNo = drvERP["PARTNO"].ToString().Trim();
            decimal QUANTITY = Commfunction.ConvertToDecimal(drvERP["QUANTITY"].ToString().Trim());
            string Unit = drvERP["UNIT"].ToString().Trim();
            string QTY_1 = drvERP["QTY_1"].ToString().Trim();                   
            string UNIT_1 = drvERP["UNIT_1"].ToString().Trim();     
            string QTY_2 = drvERP["QTY_2"].ToString().Trim();       
            string UNIT_2 = drvERP["UNIT_2"].ToString().Trim();

            string SO=drvERP["SO"].ToString().Trim();
            string Dept = drvERP["DEPT"].ToString().Trim();
            string Creater = drvERP["CREATER"].ToString().Trim();
            string Layout = drvERP["LAYOUT"].ToString().Trim();
            //string ROUTE = drvERP["ROUTE"].ToString().Trim();                 

            icb.InsertColumn("WO", WO);
            icb.InsertColumn("WO_TYPE", WO_TYPE);
            icb.InsertColumn("PARTNO", PartNo);
            icb.InsertColumn("QUANTITY", QUANTITY);
            
            icb.InsertColumn("QTY_2", Layout);
            icb.InsertColumn("UNRELEASE_QUANTITY", QUANTITY);
            icb.InsertColumn("ERP_QUANTITY", QUANTITY);
            icb.InsertColumn("SO", SO);
            icb.InsertColumn("ATTRIBUTE_04", Dept);
            if (PartNo.StartsWith("21"))
            {
                icb.InsertColumn("UNIT", "粒");
                icb.InsertColumn("UNIT_1","片" );
                icb.InsertColumn("UNIT_2", "粒");
            }
            else if (PartNo.StartsWith("22"))
            {
                icb.InsertColumn("UNIT", "粒");
                icb.InsertColumn("UNIT_1", "粒");
                icb.InsertColumn("UNIT_2", "粒");
            }
            else
            {
                icb.InsertColumn("UNIT", Unit);
                icb.InsertColumn("UNIT_1", UNIT_1);
                icb.InsertColumn("UNIT_2", UNIT_2);
            }
            if (Layout.Trim().Length>0)
            {
                QTY_1= Math.Ceiling(Convert.ToDouble(QUANTITY) / Convert.ToDouble(Layout)).ToString();
            }
            icb.InsertColumn("QTY_1", QTY_1);
            #region Assign Default Route & Product & Part
            MESFunction mes = new MESFunction(dbo);
            DataView dvPartNo = mes.GetPartNoData(PartNo);

            if (dvPartNo != null && dvPartNo.Count > 0)
            {
                icb.InsertColumn("PARTNO_VER_SID", dvPartNo[0]["PARTNO_VER_SID"].ToString());
                icb.InsertColumn("PARTNO_VERSION", dvPartNo[0]["VERSION"].ToString());

                DataView dvProduct = mes.GetProductData(dvPartNo[0]["PRODUCT_SID"].ToString().Trim());
                if (dvProduct != null)
                {
                    icb.InsertColumn("PRODUCT_SID", dvPartNo[0]["PRODUCT_SID"].ToString());
                    icb.InsertColumn("PRODUCT", dvProduct[0]["PRODUCT"].ToString());
                }
            }
            if (WO_TYPE == "Normal")
            {
                DataView dvRoute = mes.GetPartNoDefaultRouteData(dvPartNo[0]["PARTNO_VER_SID"].ToString());
                if (dvRoute != null && dvRoute.Count > 0)
                {
                    icb.InsertColumn("ROUTE_VER_SID", dvRoute[0]["ROUTE_VER_SID"].ToString());
                    icb.InsertColumn("ROUTE", dvRoute[0]["ROUTE_NAME"].ToString());
                    icb.InsertColumn("ROUTE_VERSION", dvRoute[0]["VERSION"].ToString());
                }
            }
            #endregion
            
            DateTime dt = DateTime.Now;
          
            //其他欄位預設值
            icb.InsertColumn("WO_SID", Guid.NewGuid().ToString()); 
            icb.InsertColumn("STATUS", "Create");
            icb.InsertColumn("PRIORITY", "3"); 
            //icb.InsertColumn("WO_TYPE", "Normal");
            icb.InsertColumn("UNRELEASE_QTY_1", 0);
            icb.InsertColumn("UNRELEASE_QTY_2", 0);
            icb.InsertColumn("LOT_SIZE", 1); 
            icb.InsertColumn("CREATE_USER", Creater);
            icb.InsertColumn("CREATE_DATE", dt);
            icb.InsertColumn("UPDATE_USER", WriteUserID);
            icb.InsertColumn("UPDATE_DATE", dt);

            return dbo.ExcuteCommand(icb.GetSqlCommand());
        }

        /// <summary>
        /// 更新工單資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int UpdateWOData(DataBaseObject dbo, DataRowView drvERP, string WO)
        {
            // 異動資料表名稱
            string tableName = "WP_WO";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);
 
            // 建立異動條件: STATUS="Create" 
            ucb.WhereAnd("WO", WO);
            ucb.WhereAnd("STATUS", SQLOperator.Equal, "Create");

            // 解析對應欄位 

            string WO_TYPE = drvERP["WO_TYPE"].ToString().Trim();
            string PartNo = drvERP["PARTNO"].ToString().Trim();
            decimal QUANTITY = Commfunction.ConvertToDecimal(drvERP["QUANTITY"].ToString().Trim());
            string Unit = drvERP["UNIT"].ToString().Trim();
            string QTY_1 = drvERP["QTY_1"].ToString().Trim();
            string UNIT_1 = drvERP["UNIT_1"].ToString().Trim();
            string QTY_2 = drvERP["QTY_2"].ToString().Trim();
            string UNIT_2 = drvERP["UNIT_2"].ToString().Trim();

            ucb.UpdateColumn("WO_TYPE", WO_TYPE);
            ucb.UpdateColumn("PARTNO", PartNo);

            #region Assign Default Route & Product & Part
            MESFunction mes = new MESFunction(dbo);
            DataView dvPartNo = mes.GetPartNoData(PartNo);

            if (dvPartNo != null && dvPartNo.Count > 0)
            {
                ucb.UpdateColumn("PARTNO_VER_SID", dvPartNo[0]["PARTNO_VER_SID"].ToString());
                ucb.UpdateColumn("PARTNO_VERSION", dvPartNo[0]["VERSION"].ToString());

                DataView dvProduct = mes.GetProductData(dvPartNo[0]["PRODUCT_SID"].ToString().Trim());
                if (dvProduct != null)
                {
                    ucb.UpdateColumn("PRODUCT_SID", dvPartNo[0]["PRODUCT_SID"].ToString());
                    ucb.UpdateColumn("PRODUCT", dvProduct[0]["PRODUCT"].ToString());
                }
            }
            if (WO_TYPE == "Normal")
            {
                DataView dvRoute = mes.GetPartNoDefaultRouteData(dvPartNo[0]["PARTNO_VER_SID"].ToString());
                if (dvRoute != null && dvRoute.Count > 0)
                {
                    ucb.UpdateColumn("ROUTE_VER_SID", dvRoute[0]["ROUTE_VER_SID"].ToString());
                    ucb.UpdateColumn("ROUTE", dvRoute[0]["ROUTE_NAME"].ToString());
                    ucb.UpdateColumn("ROUTE_VERSION", dvRoute[0]["VERSION"].ToString());
                }
            }
            #endregion
                         
            ucb.UpdateColumn("UNIT", Unit);
            ucb.UpdateColumn("QUANTITY", QUANTITY);
            ucb.UpdateColumn("QTY_1", QTY_1);
            ucb.UpdateColumn("UNIT_1", UNIT_1);
            ucb.UpdateColumn("QTY_2", QTY_2);
            ucb.UpdateColumn("UNIT_2", UNIT_2);
            ucb.UpdateColumn("UNRELEASE_QUANTITY", QUANTITY);
            ucb.UpdateColumn("ERP_QUANTITY", QUANTITY);

            DateTime dt = DateTime.Now;

            //其他欄位預設值           
            ucb.UpdateColumn("UPDATE_USER", WriteUserID);
            ucb.UpdateColumn("UPDATE_DATE", dt);
                 
            //將主檔放入集合內
            List<object> ins = new List<object>();
            ins.Add(ucb.GetSqlCommand());
            
            return dbo.ExcuteCommand(ins); 
        }

        /// <summary>
        /// 更新Release状态工單資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int UpdateWODataRelease(DataBaseObject dbo, DataRowView drvERP, string WO)
        {
            // 異動資料表名稱
            string tableName = "WP_WO";

            string sql = string.Format(@"SELECT * FROM WP_WO where WO = '{0}'", WO);
            DataView dvWO = dboMES.GetData(sql);

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件: STATUS="Release" 
            ucb.WhereAnd("WO", WO);
            ucb.WhereAnd("STATUS", SQLOperator.Equal, "Release");

            // 解析對應欄位 
            decimal Qty = Commfunction.ConvertToDecimal(drvERP["QUANTITY"].ToString().Trim());

            if (Commfunction.ConvertToDecimal(dvWO.Table.Rows[0]["QUANTITY"].ToString()) < Qty)
            {
                decimal qty = Commfunction.ConvertToDecimal(dvWO.Table.Rows[0]["QUANTITY"].ToString()) + (Qty - Commfunction.ConvertToDecimal(dvWO.Table.Rows[0]["QUANTITY"].ToString()));
                decimal rurelease_qty = Commfunction.ConvertToDecimal(dvWO.Table.Rows[0]["UNRELEASE_QUANTITY"].ToString()) + (Qty - Commfunction.ConvertToDecimal(dvWO.Table.Rows[0]["QUANTITY"].ToString()));

                ucb.UpdateColumn("QUANTITY", qty);
                ucb.UpdateColumn("UNRELEASE_QUANTITY", rurelease_qty);
                ucb.UpdateColumn("ERP_QUANTITY", qty);
            }

            DateTime dt = DateTime.Now;

            //其他欄位預設值           
            ucb.UpdateColumn("UPDATE_USER", WriteUserID);
            ucb.UpdateColumn("UPDATE_DATE", dt);

            //將主檔放入集合內
            List<object> ins = new List<object>();
            ins.Add(ucb.GetSqlCommand());

            return dbo.ExcuteCommand(ins);
        }

        /// <summary>
        /// 更新ERP處理狀態
        /// </summary>       
        public void UpdateERPState(DataBaseObject dbo, string WO, string State)
        {
            // 異動資料表名稱
            string tableName = "MES_WO";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("WO", WO);

            ucb.UpdateColumn("STATE", State);

            dbo.ExcuteCommand(ucb.GetSqlCommand());
        }

        #endregion 
         
        private string WriteAlarmJob(DataBaseObject dbo)
        {          
            string SendSubject = "[EIS]轉檔錯誤通知--工單";
            string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;

            return MESFunction.WriteAlarmJob(dbo, SendSubject, SendFilePath);
        }
         
        public void Start(string[] args)
        {            
            nlog = LogManager.GetLogger(this.GetType().Name);

            DateTime Job_StartTime = DateTime.Now;

            //Keep Process SID, 檢測每次轉檔執行緒
            string EIS_SID = "WO";
            LogAction LogAction;

            // 記錄是否發送Email
            bool AlarmJobFlag = false;

            DateTime EISOldLastUpdate = DateTime.Now;

            try
            {                
                if (args.Length > 0)
                {
                    EIS_SID = args[0].Trim();
                }

                // 連線資料庫
                ConnectDataBase();

                // 記錄Log : 開始轉料號
                TraceNormalLog("Start Sync !");

                #region 判斷是否有multiProcess狀況

                bool multiProcess = false;
                string[] Condition = null;

                //取出EIS_JOB執行狀況,回傳至EISResult  
                dboEIS.BeginTransaction();
                EISResult EIS = EISFunction.CheckEISResult(dboEIS, EIS_SID, Condition, EISOldLastUpdate);
                dboEIS.Commit();

                multiProcess = EIS.multiProcess;
                Condition = EIS.Condition;
                EISOldLastUpdate = EIS.EISOldLastUpdate;

                if (multiProcess)
                {
                    // 當 JOB 狀態為 Run, 表示有平行處理的問題 
                    // 新增紀錄Log (EIS_JOBHIST), 不更新JOB CONTROL 
                    dboEIS.BeginTransaction();
                    int iLogCount =
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[WO] Job State=Run, Muti Process!!");
                    dboEIS.Commit();

                    TraceNormalLog("JOB 狀態為 Run ,可能有其他相同程式也在執行");
                     
                    return;
                }

                #endregion

                #region Get ERP Data

                //取得ERP來源資料
                DataView dvERP = GetERPData(Condition);

                #endregion
                 
                #region Parser ERP Data

                // 記錄Log : 開始解析ERP來源料號 
                TraceNormalLog("Start Parser : " + dvERP.Count.ToString());

                string NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                string LogProcessDesc = string.Empty;
                
                if (Condition == null)
                {
                    //第一次同步
                    LogProcessDesc = "First Sync";
                    LogAction = LogAction.Insert;
                }
                else
                {
                    //有上次查詢條件
                    LogProcessDesc = "Data Sync";
                    LogAction = LogAction.DataSync;
                }

                int iRowCount = 0;
                int iInsertCount = 0;
                int iUpdateCount = 0;

                if (dvERP != null && dvERP.Count > 0)   
                {
                    #region 有異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    MESFunction mes = new MESFunction(dboMES);
                    int iUpdSuccess = 0;
                    int iInsSuccess = 0;
                    string sWO = string.Empty;

                    dboMES.BeginTransaction();

                    for (int i = 0; i < dvERP.Count; i++)
                    {
                        iUpdSuccess = 0;
                        iInsSuccess = 0;
                        sWO = dvERP[i]["WO"].ToString().Trim();

                        //檢查是否已有WO
                        if (mes.IsExistWO(sWO))
                        {
                            try
                            {
                                string sWoStatus = mes.GetWOStatus(sWO);

                                if (sWoStatus != "Create" && sWoStatus != "Release")
                                {
                                    UpdateERPState(dboERP, sWO,"2");
                                    // 記錄Log 
                                    TraceNormalLog("UPDATE|" + sWO + " ex:MES工單狀態非Create，Release，不允許變更。");
                                }
                                else
                                {
                                    if (sWoStatus == "Release")
                                    {
                                        //回傳UPDATE成功筆數                               
                                        iUpdSuccess = UpdateWODataRelease(dboMES, dvERP[i], sWO);
                                    }
                                    else
                                    {
                                        //回傳UPDATE成功筆數                               
                                        iUpdSuccess = UpdateWOData(dboMES, dvERP[i], sWO);
                                    }
                                    if (iUpdSuccess > 0)
                                    {
                                        iUpdateCount += 1;
                                        //Console.WriteLine(" U " + iUpdateCount.ToString() + ".WO :" + sWO);
                                        UpdateERPState(dboERP, sWO, "1");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                UpdateERPState(dboERP, sWO, "2");

                                // 記錄Log
                                TraceNormalLog("UPDATE|" + sWO +  " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }
                        else
                        {
                            try
                            {
                                //回傳INSERT成功筆數 
                                iInsSuccess = WriteWOData(dboMES, dvERP[i]);
                                if (iInsSuccess > 0)
                                {
                                    iInsertCount += 1;
                                    //Console.WriteLine(" I " + iInsertCount.ToString() + ".WO :" + sWO);
                                    UpdateERPState(dboERP, sWO, "1");
                                }
                            }
                            catch (Exception ex)
                            {
                                UpdateERPState(dboERP, sWO, "2");

                                // 記錄Log 
                                TraceNormalLog("UPDATE|" + sWO +  " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }
                    }
                
                    iRowCount = iUpdateCount + iInsertCount;
                    LogProcessDesc += ": Check Row=" + dvERP.Count + ", Update Row=" + iUpdateCount + ", Insert Row=" + iInsertCount;
                    //NextUpdateDate = dvERP[dvERP.Count - 1]["MODIFY_TIME"].ToString();
                    //if (NextUpdateDate.Equals(""))
                        NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                    
                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, iRowCount, "[WO] " + LogProcessDesc);
                    int EIS_JobControlCount = 0;

                    if (AlarmJobFlag == false)
                    {
                        // *** 重要 *** 
                        // 更新 EIS_JOBCONTROL,記錄下次轉檔的比對時間(NextUpdateDate) 
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, NextUpdateDate, EISOldLastUpdate);
                    }
                    else
                    { 
                        //更新 EIS_JOBCONTROL, 切換State回Idle  
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);                        
                    }

                    if (EIS_LogCount > 0 && EIS_JobControlCount > 0)
                    {
                        try
                        {
                            dboEIS.Commit();
                            dboMES.Commit(); 
                        }
                        catch (Exception ex)
                        {
                            TraceErrorLog(ex);
                            dboMES.Rollback();
                        }
                    }
                    else
                    {
                        dboEIS.Commit();
                        dboMES.Rollback();
                    }

                    #endregion
                }
                else
                {
                    #region 無異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    LogProcessDesc += ": 無異動紀錄";

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, 0, "[WO] " + LogProcessDesc);

                    //更新 EIS_JOBCONTROL, 並記錄下次轉檔的比對時間 
                    int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, Condition[0].Trim(), EISOldLastUpdate);

                    dboEIS.Commit();

                    #endregion
                }

                #endregion
                 
                Console.WriteLine("End Parser : " + LogProcessDesc);

                // 記錄Log : 結束解析ERP來源料號 
                TraceNormalLog("End Parser : " + LogProcessDesc);
                TraceNormalLog("End Sync !");
                TraceNormalLog("-------------------------------------------------------");

                if (AlarmJobFlag)
                {
                    // 寫入Alarm發送EMail
                    string AlarmMsg = WriteAlarmJob(dboMES);
                    if (!AlarmMsg.Equals("Success"))
                    {
                        dboEIS.BeginTransaction();
                        //新增Log (EIS_JOBHIST)
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Alarm] 有錯誤發生, ex:" + AlarmMsg);
                        dboEIS.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                TraceErrorLog(e);

                dboEIS.BeginTransaction();
                //更新 EIS_JOBCONTROL, 切換State回Idle  
                int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                dboEIS.Commit();
            }
            finally
            {
                if (dboEIS != null)
                {
                    dboEIS.ColseConnect();
                }
                if (dboERP != null)
                {
                    dboERP.ColseConnect();
                }
                if (dboMES != null)
                {
                    dboMES.ColseConnect();
                }
            }
        }
         
    }
}