﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;
using Genesis.Gtimes.Common;
using Genesis.Gtimes.ADM;
using Genesis.Gtimes.Transaction;
using Genesis.Gtimes.WIP;


namespace Genesis.Gtimes.EIS
{
    public class EISQCCheck : EISBase
    {
        #region 轉換欄位資訊
        public DBController eisDbc = new DBController(ConfigurationManager.ConnectionStrings["MES.SqlServer"]);
       /// <summary>
        /// 取得ERP來源資料
        /// </summary>
        /// <returns></returns>
        public DataTable GetQCData()
        {
            string SQL = string.Empty;


            SQL = @"select  distinct MAPP.EQP_SID from EQP_MAPPING Mapp,FC_EQUIPMENT EQP
                    where
					MAPP.EQP_SID=EQP.EQP_SID AND eqp.STATE_NO <>'Down' AND
                    (((CLT_STATUS=1 OR CLT_STATUS=2)
                    AND CLT_UPDATE_DATE<:NowTime )
                    OR
                    ((CLT_TOOL_STATUS=1 OR CLT_TOOL_STATUS=2)
                    AND CLT_TOOL_UPDATE_DATE< :NowTime ))";
            DateTime NowTime = DateTime.Now.AddMinutes(30);
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            eisDbc.AddCommandParameter(parameters, "NowTime", NowTime);
            DataTable dtQC = eisDbc.Select(SQL, parameters);
            return dtQC;
        }

      

        #endregion

        private string WriteAlarmJob(DataBaseObject dbo)
        {
            string SendSubject = "[EIS]首件检查延误通知";
            string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;

            return MESFunction.WriteAlarmJob(dbo, SendSubject, SendFilePath);
        }
        private DataTable getEqpLot(EquipmentUtility.EquipmentInfo eqpInfo)
        {
            string sql = string.Empty;
            sql = @"select lot from WP_LOT lot,WP_EQP_TRACE trace
                        where lot.EQP_LINK_SID=trace.ACTION_LINK_SID
                        and trace.EQP_NO=:eqpno";
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            eisDbc.AddCommandParameter(parameters, "eqpno", eqpInfo.No);
            DataTable dt = eisDbc.Select(sql, parameters);
            return dt;
        }
        private List<IDbCommand> getAlarm(DateTime txnTime,string MailAddress,string EqpNo)
        {
            string mailSID = eisDbc.GetSID();
            List<IDbCommand> commands = new List<IDbCommand>();
            Genesis.Gtimes.Common.InsertCommandBuilder insertALM = new Genesis.Gtimes.Common.InsertCommandBuilder(eisDbc, "AL_JOB");
            insertALM.InsertColumn("SID", mailSID);
            insertALM.InsertColumn("SENDTYPE", "MAIL");
            insertALM.InsertColumn("SENDTIME", txnTime);
            insertALM.InsertColumn("SENDSUBJECT", "首件检查通知");
            insertALM.InsertColumn("SNEDBODY", "机台" + EqpNo + "首件检查的QC确认超时,已经将机台的状态修改为Down。");
            insertALM.InsertColumn("SENDNUM", "0");
            insertALM.InsertColumn("SENDSTATUS", "0");
            insertALM.InsertColumn("REMARKS", "");
            commands.Add(insertALM.GetCommand());

            Genesis.Gtimes.Common.InsertCommandBuilder insertALMMail = new Genesis.Gtimes.Common.InsertCommandBuilder(eisDbc, "AL_JOB_DETAIL");
            insertALMMail.InsertColumn("SID", eisDbc.GetSID());
            insertALMMail.InsertColumn("AL_JOB_SID", mailSID);
            insertALMMail.InsertColumn("SEND_TIME", txnTime);
            insertALMMail.InsertColumn("MAIL_ADDRESS", MailAddress);
            insertALMMail.InsertColumn("SEND_NUM", "0");
            insertALMMail.InsertColumn("STATUS", "0");
            commands.Add(insertALMMail.GetCommand());
            return commands;
        }
        public void Start(string[] args)
        {
            nlog = LogManager.GetLogger(this.GetType().Name);

            DateTime Job_StartTime = DateTime.Now;

            //Keep Process SID, 檢測每次轉檔執行緒
            string EIS_SID = "EISQCCheck";
            LogAction LogAction;

            // 記錄是否發送Email
            bool AlarmJobFlag = false;

            DateTime EISOldLastUpdate = DateTime.Now;

            try
            {
                if (args.Length > 0)
                {
                    EIS_SID = args[0].Trim();
                }

                // 連線資料庫
                //ConnectDataBase();
                #region EIS Connection

                DataBaseType EISdatabaseType = Commfunction.ConvertToDataBaseType(ConfigurationManager.AppSettings["EIS.DBType"].ToString().Trim());
                string EISConnString = "";
                if (EISdatabaseType == DataBaseType.SqlServer)
                {
                    EISConnString = ConfigurationManager.ConnectionStrings["EIS.SqlServer"].ToString();
                }
                else
                {
                    EISConnString = ConfigurationManager.ConnectionStrings["EIS.Oracle"].ToString();
                }
                dboEIS = new DataBaseObject(EISdatabaseType);
                dboEIS.OpenConnect(EISConnString);
                //dboEIS.BeginTransaction();

                Console.WriteLine("Connect EIS OK !");
                #endregion

                // 記錄Log : 開始轉料號
                TraceNormalLog("Start Sync !");

                #region 判斷是否有multiProcess狀況

                bool multiProcess = false;
                string[] Condition = null;

                //取出EIS_JOB執行狀況,回傳至EISResult  
                dboEIS.BeginTransaction();
                EISResult EIS = EISFunction.CheckEISResult(dboEIS, EIS_SID, Condition, EISOldLastUpdate);
                dboEIS.Commit();

                multiProcess = EIS.multiProcess;
                Condition = EIS.Condition;
                EISOldLastUpdate = EIS.EISOldLastUpdate;

                if (multiProcess)
                {
                    // 當 JOB 狀態為 Run, 表示有平行處理的問題 
                    // 新增紀錄Log (EIS_JOBHIST), 不更新JOB CONTROL 
                    dboEIS.BeginTransaction();
                    int iLogCount =
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[WO] Job State=Run, Muti Process!!");
                    dboEIS.Commit();

                    TraceNormalLog("JOB 狀態為 Run ,可能有其他相同程式也在執行");

                    return;
                }

                #endregion

                #region Get QC Data

                //取得來源資料
                DataTable  dtQC = GetQCData();

                #endregion

                #region Parser ERP Data
              
                LogAction = LogAction.DataSync;
                string NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                string LogProcessDesc = string.Empty;
                LogProcessDesc = "Data Sync";

                if (dtQC != null && dtQC.Rows.Count > 0)
                {
                    // 記錄Log : 開始解析ERP來源料號 
                    TraceNormalLog("Start Parser : " + dtQC.Rows.Count.ToString());

                 


                    //有上次查詢條件
               
                    
                    #region 有異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)
                    int HoldLotCount = 0;
                    string linkSID = this.eisDbc.GetSID();
                    DateTime txnTime = this.eisDbc.GetDBTime();
                    TransactionUtility.TransactionBase txnBase = new TransactionUtility.TransactionBase(linkSID, "EISCheck", txnTime,"EISQCHold","", "EISQCCheck");
                    TransactionUtility.GtimesTxn gtimesTxn = new TransactionUtility.GtimesTxn(eisDbc, txnBase);
                    TransactionUtility.AddSQLCommandTxn commandTxn = new TransactionUtility.AddSQLCommandTxn();
                    List<IDbCommand> commands = new List<IDbCommand>();
                    EquipmentUtility.EquipmentStateInfo eqpstatus=new EquipmentUtility.EquipmentStateInfo (eisDbc,"Down",EquipmentUtility.IndexType.No);
                    ParameterUtility.ParameterInfo ParaInfo = new ParameterUtility.ParameterInfo(eisDbc, "QCMailAddress", ParameterUtility.ParameterInfo.IndexType.No);
                    string MailAddress = string.Empty
                        ;
                    if (ParaInfo.IsExist == false)
                    {
                        MailAddress = "1234567890@qq.com";
                    }
                    else
                    {
                        MailAddress = ParaInfo.PARAMETER_VALUE;
                    }
                    using (IDbTransaction tx = eisDbc.GetTransaction())
                    {
                        try
                        {
                            for (int i = 0; i < dtQC.Rows.Count; i++)
                            {
                                EquipmentUtility.EquipmentInfo eqpinfo = new EquipmentUtility.EquipmentInfo(eisDbc, dtQC.Rows[i]["EQP_SID"].ToString(), EquipmentUtility.IndexType.SID);
                                if (eqpinfo.IsExist == false) continue;
                                if (eqpinfo.STATE_NO == "Run")
                                {
                                    DataTable dtLot = getEqpLot(eqpinfo);
                                    if (dtLot == null || dtLot.Rows.Count == 0) continue;
                                    for (int j = 0; j < dtLot.Rows.Count; j++)
                                    {
                                        LotUtility.LotInfo lot = new LotUtility.LotInfo(eisDbc, dtLot.Rows[j]["LOT"].ToString(), LotUtility.IndexType.NO);
                                        Transaction.WIP.WIPTransaction.HoldLotTxn holdLot = new Genesis.Gtimes.Transaction.WIP.WIPTransaction.HoldLotTxn(lot);
                                        gtimesTxn.Add(holdLot);
                                        Transaction.WIP.WIPTransaction.EndOfLotTxn endLot = new Genesis.Gtimes.Transaction.WIP.WIPTransaction.EndOfLotTxn(lot);
                                        gtimesTxn.Add(endLot);
                                    }
                                }
                                Transaction.EQP.EQPTransaction.EquipmentChangeStateTxn oChangeStatus = new Genesis.Gtimes.Transaction.EQP.EQPTransaction.EquipmentChangeStateTxn(eqpinfo, eqpstatus);
                                gtimesTxn.Add(oChangeStatus);
                                
                                commandTxn.Commands.AddRange( getAlarm(txnTime,MailAddress,eqpinfo.No));
                                gtimesTxn.Add(commandTxn);
                                gtimesTxn.DoTransaction(gtimesTxn.GetTransactionCommands(), tx);
                                gtimesTxn.Clear();
                                commandTxn.Commands.Clear();
                            }
                            tx.Commit();
                        }
                        catch (Exception txnEX)
                        {
                            tx.Rollback();
                            throw txnEX;
                        }
                    }
                  
                    LogProcessDesc += ": Change Eqp Row=" + dtQC.Rows.Count + " Hold Lot Row=" + HoldLotCount.ToString();
                   
                    NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, dtQC.Rows.Count, "[EQP] " + LogProcessDesc);
                    int EIS_JobControlCount = 0;

                    if (AlarmJobFlag == false)
                    {
                        // *** 重要 *** 
                        // 更新 EIS_JOBCONTROL,記錄下次轉檔的比對時間(NextUpdateDate) 
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, NextUpdateDate, EISOldLastUpdate);
                    }
                    else
                    {
                        //更新 EIS_JOBCONTROL, 切換State回Idle  
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                    }

                    if (EIS_LogCount > 0 && EIS_JobControlCount > 0)
                    {
                        try
                        {
                            dboEIS.Commit();
                        }
                        catch (Exception ex)
                        {
                            TraceErrorLog(ex);
                        }
                    }
                    else
                    {
                        dboEIS.Commit();
                    }

                    #endregion
                }
                else
                {
                    #region 無異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    LogProcessDesc += ": 無異動紀錄";

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, 0, "[WO] " + LogProcessDesc);

                    //更新 EIS_JOBCONTROL, 並記錄下次轉檔的比對時間 
                    int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, Condition[0].Trim(), EISOldLastUpdate);

                    dboEIS.Commit();

                    #endregion
                }

                #endregion

                Console.WriteLine("End Parser : " + LogProcessDesc);

                // 記錄Log : 結束解析ERP來源料號 
                TraceNormalLog("End Parser : " + LogProcessDesc);
                TraceNormalLog("End Sync !");
                TraceNormalLog("-------------------------------------------------------");

                //if (AlarmJobFlag)
                //{
                //    // 寫入Alarm發送EMail
                //    string AlarmMsg = WriteAlarmJob(dboMES);
                //    if (!AlarmMsg.Equals("Success"))
                //    {
                //        dboEIS.BeginTransaction();
                //        //新增Log (EIS_JOBHIST)
                //        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Alarm] 有錯誤發生, ex:" + AlarmMsg);
                //        dboEIS.Commit();
                //    }
                //}
            }
            catch (Exception e)
            {
                TraceErrorLog(e);

                dboEIS.BeginTransaction();
                //更新 EIS_JOBCONTROL, 切換State回Idle  
                int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                dboEIS.Commit();
            }
            finally
            {
                if (dboEIS != null)
                {
                    dboEIS.ColseConnect();
                }
              
            }
        }

    }
}