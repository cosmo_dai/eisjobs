﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OracleClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;

namespace Genesis.Gtimes.EIS
{
    public class EISToPartNo : EISBase
    {  
        #region 轉換欄位資訊

        /// <summary>
        /// 取得ERP來源資料
        /// </summary>
        /// <returns></returns>
        public DataView GetERPData(string[] Condition)
        {
            string ERPSQL = string.Empty;

            //if (Condition == null)
            //{
                // 第一次同步 
                ERPSQL = @" 
                Select   PARTNO,PART_NAME,UNIT,PARTNO_TYPE,PART_SPEC   
                From MES_PART where STATE = 0 ";
//            }
//            else
//            {
//                // 有上次查詢條件
//                string LastModifyDate = Condition[0].Trim();
                 
//                ERPSQL = string.Format(@"
//                SELECT * FROM (
//                    Select  PARTNO,PART_NAME,UNIT,PARTNO_TYPE,PART_SPEC
//                            , Modified_Time as MODIFY_TIME      
//                    From MES_PART where STATE = 0 
//                ) a 
//             
//                ORDER BY MODIFY_TIME
//                ", LastModifyDate);
//            }

            DataView dvERP = dboERP.GetData(ERPSQL);
            return dvERP;
        }
 
        /// <summary>
        /// 新增料號基本資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int WritePartNoData(DataBaseObject dbo, DataRowView drvERP)
        {
            // 新增資料表名稱
            string tableName = "PF_PARTNO";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);
            
            // 解析對應欄位 
            string PartNoSID = Guid.NewGuid().ToString();
            string PartNo = drvERP["PARTNO"].ToString().Trim();
            //string PartName = drvERP["PART_NAME"].ToString().Trim();
            string PART_SPEC = drvERP["PART_SPEC"].ToString().Trim();
            string PartName = drvERP["PART_NAME"].ToString().Trim() + "<[(" + drvERP["PARTNO"].ToString().Trim() + ")]>";
           
            //string PART_SPEC = drvERP["PART_NAME"].ToString().Trim()+"  "+drvERP["PART_SPEC"].ToString().Trim();
           
            string PartType = drvERP["PARTNO_TYPE"].ToString().Trim();

            //string VersionSpec = drvERP["Inv_Spec"].ToString().Trim();
            //VersionSpec = (VersionSpec.Length == 0 ? "NULL" : VersionSpec);
            //string ERP_Category = drvERP["CATEGORY"].ToString().Trim();
            //string Category = string.Empty;
            string Unit = drvERP["UNIT"].ToString().Trim();
            string ProductSID = "GTI13041716461881915";
            string PartTypeSID = "GTI15100513322100178";
            
            icb.InsertColumn("PARTNO_SID", PartNoSID);
            icb.InsertColumn("PARTNO", PartNo);
            icb.InsertColumn("PART_NAME", PartName);
            icb.InsertColumn("VERSION_SPEC", PART_SPEC);
            icb.InsertColumn("ATTRIBUTE_01", drvERP["PART_NAME"].ToString().Trim());
            #region 取 PartType & Product
            MESFunction mes = new MESFunction(dbo);
            //系統預設料號類別
            DataView dvPartType = mes.GetPartTypeDataByNO(PartType);
            if (dvPartType != null && dvPartType.Count > 0)
            {
                PartTypeSID = dvPartType[0]["PARTNO_TYPE_SID"].ToString().Trim();
            }
            else
            {
                dvPartType = mes.GetPartTypeData();
                if (dvPartType != null && dvPartType.Count > 0)
                    PartTypeSID = dvPartType[0]["PARTNO_TYPE_SID"].ToString().Trim();
            }
            //系統預設產品
            DataView dvProduct = mes.GetProductData();
            if (dvProduct != null && dvProduct.Count > 0)
                ProductSID = dvProduct[0]["PRODUCT_SID"].ToString().Trim();             
            #endregion 
            icb.InsertColumn("PRODUCT_SID", ProductSID);
            icb.InsertColumn("PARTNO_TYPE_SID", PartTypeSID);

            //其他欄位預設值            
            icb.InsertColumn("DEFAULT_VERSION", 1);
            icb.InsertColumn("MAX_VERSION", 1);

            //if (ERP_Category == "FERT") Category = "Product";
            //else if (ERP_Category == "HALB") Category = "Semi-Product";
            //else if (ERP_Category == "ROH") Category = "Purchase";
            icb.InsertColumn("CATEGORY", "Product");
            
            icb.InsertColumn("ABC_CODE",  "C");
            icb.InsertColumn("LOT_CONTROL_FLAG", "F");
            icb.InsertColumn("BONDED_FLAG", "F"); 
            icb.InsertColumn("UNIT", Unit);

            DateTime dt = DateTime.Now;
          
            icb.InsertColumn("CREATE_USER", WriteUserID);
            icb.InsertColumn("CREATE_DATE", dt);
            icb.InsertColumn("UPDATE_USER", WriteUserID);
            icb.InsertColumn("UPDATE_DATE", dt);

            int iMaster = dbo.ExcuteCommand(icb.GetSqlCommand());

            if (iMaster > 0)
            {
                iMaster += WritePartNoVerData(dbo, PartNoSID, PartNo, PartName, PART_SPEC, dt, drvERP["PART_NAME"].ToString().Trim());
            }
            
            return iMaster;
        }

        /// <summary>
        /// 新增料號版本資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="PartNoSID"></param>
        /// <param name="PartNo"></param>
        /// <param name="PartName"></param>
        /// <param name="dtUpdate"></param>
        /// <returns></returns>
        public int WritePartNoVerData(DataBaseObject dbo, string PartNoSID, string PartNo, string PartName, string VersionSpec, DateTime dtUpdate,string attribute01)
        {
            // 新增資料表名稱
            string tableName = "PF_PARTNO_VER";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);

            // 解析對應欄位 
            icb.InsertColumn("PARTNO_VER_SID", Guid.NewGuid().ToString());
            icb.InsertColumn("PARTNO_SID", PartNoSID);
            icb.InsertColumn("PARTNO", PartNo);
            icb.InsertColumn("PART_NAME", PartName); 
            icb.InsertColumn("VERSION", 1);
            icb.InsertColumn("VERSION_SPEC", VersionSpec);
            icb.InsertColumn("VERSION_STATE", "Enable");
            icb.InsertColumn("DEFAULT_FLAG", "T");
            icb.InsertColumn("ENG_FLAG", "F");
            icb.InsertColumn("BOM_ENABLE_FLAG", "F");
            icb.InsertColumn("MANUFACTURE_FALG", "T");
            icb.InsertColumn("CREATE_USER", WriteUserID);
            icb.InsertColumn("CREATE_DATE", dtUpdate);
            icb.InsertColumn("UPDATE_USER", WriteUserID);
            icb.InsertColumn("UPDATE_DATE", dtUpdate);
            icb.InsertColumn("ATTRIBUTE_01", attribute01);
 
            return dbo.ExcuteCommand(icb.GetSqlCommand());
        }

        /// <summary>
        /// 更新料號資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int UpdatePartNoData(DataBaseObject dbo, DataRowView drvERP, string PartNo)
        {
            // 異動資料表名稱
            string tableName = "PF_PARTNO";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);
 
            // 建立異動條件
            ucb.WhereAnd("PartNo", PartNo);

            // 解析對應欄位 
            //string PartName = drvERP["PART_NAME"].ToString().Trim();
            //string PartName = drvERP["PARTNO"].ToString().Trim();
            string Unit = drvERP["UNIT"].ToString().Trim();
            string PartType = drvERP["PARTNO_TYPE"].ToString().Trim();
            string PART_SPEC = drvERP["PART_SPEC"].ToString().Trim();
            //string PART_SPEC = drvERP["PART_NAME"].ToString().Trim()+"   "+drvERP["PART_SPEC"].ToString().Trim();
            string PartTypeSID = "GTI15100513322100178";
            string PartName = drvERP["PART_NAME"].ToString().Trim() + "<[(" + drvERP["PARTNO"].ToString().Trim() + ")]>";
           
            ucb.UpdateColumn("PART_NAME", PartName);
            ucb.UpdateColumn("VERSION_SPEC", PART_SPEC); 
            ucb.UpdateColumn("UNIT", Unit);
            #region 取 PartType
            MESFunction mes = new MESFunction(dbo);
            //系統預設料號類別
            DataView dvPartType = mes.GetPartTypeDataByNO(PartType);
            if (dvPartType != null && dvPartType.Count > 0)
            {
                PartTypeSID = dvPartType[0]["PARTNO_TYPE_SID"].ToString().Trim();
            }
            else
            {
                dvPartType = mes.GetPartTypeData();
                if (dvPartType != null && dvPartType.Count > 0)
                    PartTypeSID = dvPartType[0]["PARTNO_TYPE_SID"].ToString().Trim();
            }
            #endregion
            ucb.UpdateColumn("PARTNO_TYPE_SID", PartTypeSID);

            ucb.UpdateColumn("ATTRIBUTE_01", drvERP["PART_NAME"].ToString().Trim());
            DateTime dt = DateTime.Now;

            //其他欄位預設值
            ucb.UpdateColumn("UPDATE_USER", WriteUserID);
            ucb.UpdateColumn("UPDATE_DATE", dt);
             
            int iMaster = dbo.ExcuteCommand(ucb.GetSqlCommand());

            if (iMaster > 0)
            {
                iMaster += UpdatePartNoVerData(dbo, PartNo, PartName, PART_SPEC, dt, drvERP["PART_NAME"].ToString().Trim());
            }

            return iMaster;
        }

        /// <summary>
        /// 更新料號版本資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="PartNo"></param>
        /// <param name="PartName"></param>
        /// <param name="dtUpdate"></param>
        /// <returns></returns>
        public int UpdatePartNoVerData(DataBaseObject dbo, string PartNo, string PartName, string VersionSpec, DateTime dtUpdate,string attribute01)
        {
            // 異動資料表名稱
            string tableName = "PF_PARTNO_VER";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("PartNo", PartNo);

            // 解析對應欄位 
            ucb.UpdateColumn("PART_NAME", PartName);
            ucb.UpdateColumn("VERSION_SPEC", VersionSpec);
            ucb.UpdateColumn("UPDATE_USER", WriteUserID);
            ucb.UpdateColumn("UPDATE_DATE", dtUpdate);
            ucb.UpdateColumn("ATTRIBUTE_01", attribute01);
            return dbo.ExcuteCommand(ucb.GetSqlCommand());
        }

        /// <summary>
        /// 更新ERP處理狀態
        /// </summary>       
        public void UpdateERPState(DataBaseObject dbo, string PartNo,string State)
        {
            // 異動資料表名稱
            string tableName = "MES_PART";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("PARTNO", PartNo);

            ucb.UpdateColumn("STATE", State);

            dbo.ExcuteCommand(ucb.GetSqlCommand());
        }

        #endregion 
         
        private string WriteAlarmJob(DataBaseObject dbo)
        {
            string SendSubject = "[EIS]轉檔錯誤通知--料號";
            string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;

            return MESFunction.WriteAlarmJob(dbo, SendSubject, SendFilePath);
        }

        public void Start(string[] args)
        {
            nlog = LogManager.GetLogger(this.GetType().Name);

            DateTime Job_StartTime = DateTime.Now;

            //Keep Process SID, 檢測每次轉檔執行緒
            string EIS_SID = "PartNo";
            LogAction LogAction;

            // 記錄是否發送Email
            bool AlarmJobFlag = false;

            DateTime EISOldLastUpdate = DateTime.Now;

            try
            {
                if (args.Length > 0)
                {
                    EIS_SID = args[0].Trim();
                }

                // 連線資料庫
                ConnectDataBase();
               
                // 記錄Log : 開始轉料號
                TraceNormalLog("Start Sync !");

                #region 判斷是否有multiProcess狀況

                bool multiProcess = false;
                string[] Condition = null;
                
                //取出EIS_JOB執行狀況,回傳至EISResult  
                dboEIS.BeginTransaction();
                EISResult EIS = EISFunction.CheckEISResult(dboEIS, EIS_SID, Condition, EISOldLastUpdate);
                dboEIS.Commit();

                multiProcess = EIS.multiProcess;
                Condition = EIS.Condition;
                EISOldLastUpdate = EIS.EISOldLastUpdate;

                if (multiProcess)
                {
                    // 當 JOB 狀態為 Run, 表示有平行處理的問題 
                    // 新增紀錄Log (EIS_JOBHIST), 不更新JOB CONTROL 
                    dboEIS.BeginTransaction();
                    int iLogCount =
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Part] Job State=Run, Muti Process!!");
                    dboEIS.Commit();

                    TraceNormalLog("JOB 狀態為 Run ,可能有其他相同程式也在執行");

                    return;
                }

                #endregion

                #region Get ERP Data

                //取得ERP來源資料
                DataView dvERP = GetERPData(Condition);
                 
                #endregion

                #region Parser ERP Data

                // 記錄Log : 開始解析ERP來源料號 
                TraceNormalLog("Start Parser : " + dvERP.Count.ToString());

                string NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                string LogProcessDesc = string.Empty;
                
                if (Condition == null)
                {
                    //第一次同步
                    LogProcessDesc = "First Sync";
                    LogAction = LogAction.Insert;
                }
                else
                {
                    //有上次查詢條件
                    LogProcessDesc = "Data Sync";
                    LogAction = LogAction.DataSync;
                }

                if (dvERP != null && dvERP.Count > 0)   
                {
                    #region 有異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)
                    
                    MESFunction mes = new MESFunction(dboMES);

                    //異動成功筆數
                    int iUpdSuccess = 0;
                    int iInsSuccess = 0;

                    //累計異動筆數
                    int iInsertCount = 0;
                    int iUpdateCount = 0;

                    //累計達N筆,執行Commit
                    int iCommitCount = 0;

                    //檢查KEY
                    string sPartNo = string.Empty;

                    dboMES.BeginTransaction();

                    for (int i = 0; i < dvERP.Count; i++)
                    {
                        iUpdSuccess = 0;
                        iInsSuccess = 0;
                        sPartNo = dvERP[i]["PARTNO"].ToString().Trim();
                        
                        //檢查是否已有PartNo
                        if (mes.IsExistPartNo(sPartNo))
                        {
                            try
                            {
                                //回傳UPDATE成功筆數 
                                iUpdSuccess = UpdatePartNoData(dboMES, dvERP[i], sPartNo);
                                if (iUpdSuccess > 0)
                                {
                                    iUpdateCount += 1;
                                    iCommitCount += 1;
                                    //Console.WriteLine(" U " + iUpdateCount.ToString() + ".PartNo :" + sPartNo);
                                    UpdateERPState(dboERP, sPartNo, "1");
                                } 
                             }
                            catch (Exception ex)
                            {
                                UpdateERPState(dboERP, sPartNo, "2");

                                // 記錄Log
                                TraceNormalLog("UPDATE|" + sPartNo +  " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }
                        else
                        {
                            try
                            {
                                //回傳INSERT成功筆數 
                                iInsSuccess = WritePartNoData(dboMES, dvERP[i]);
                                if (iInsSuccess > 0)
                                {
                                    iInsertCount += 1;
                                    iCommitCount += 1;
                                    //Console.WriteLine(" I " + iInsertCount.ToString() + ".PartNo :" + sPartNo);
                                    UpdateERPState(dboERP, sPartNo, "1");
                                }
                            }
                            catch (Exception ex)
                            {
                                UpdateERPState(dboERP, sPartNo, "2");

                                // 記錄Log 
                                TraceNormalLog("INSERT|" + sPartNo + " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }

                        //不管新增或異動,每3000就Commit
                        if (iCommitCount >= 3000)
                        {
                            try
                            {
                                dboMES.Commit();
                                TraceNormalLog("寫入異動資料" + iCommitCount.ToString() + "筆!");

                                dboMES.BeginTransaction();
                                iCommitCount = 0;  //清除重來

                                //NextUpdateDate = dvERP[i]["MODIFY_TIME"].ToString();
                                //if (NextUpdateDate.Equals("")) 
                                    NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                               
                                // *** 重要 *** 
                                // 記錄下次轉檔的比對時間(NextUpdateDate),State不變
                                dboEIS.BeginTransaction();
                                int iEIS_1 = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Run, NextUpdateDate, EISOldLastUpdate);
                                dboEIS.Commit();
                            }
                            catch (Exception ex)
                            {
                                TraceErrorLog(ex);
                                dboMES.Rollback();
                            }
                        }
                    }

                    //餘數未Commit
                    if (iCommitCount < 3000)
                    {
                        try
                        {
                            dboMES.Commit();
                            TraceNormalLog("寫入異動資料餘數" + iCommitCount.ToString() + "筆!");
                            iCommitCount = 0;
                        }
                        catch (Exception ex)
                        {
                            TraceErrorLog(ex);
                            dboMES.Rollback();
                        }
                    }

                    if (AlarmJobFlag == false)
                    {
                        int iRowCount = iUpdateCount + iInsertCount;
                        LogProcessDesc += ": Check Row=" + dvERP.Count + ", Update Row=" + iUpdateCount + ", Insert Row=" + iInsertCount;
                     
                        //NextUpdateDate = dvERP[dvERP.Count - 1]["MODIFY_TIME"].ToString();                        
                        //if (NextUpdateDate.Equals(""))
                            NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                        
                        dboEIS.BeginTransaction();

                        //新增Log (EIS_JOBHIST)
                        int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, iRowCount, "[Part] " + LogProcessDesc);
                      
                        // *** 重要 *** 
                        // 更新 EIS_JOBCONTROL,記錄下次轉檔的比對時間(NextUpdateDate) 
                        int  EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, NextUpdateDate, EISOldLastUpdate);
                        
                        dboEIS.Commit();
                    }
                    else
                    {
                        dboEIS.BeginTransaction();
                        //更新 EIS_JOBCONTROL, 切換State回Idle  
                        int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                        dboEIS.Commit();
                    }
                    #endregion
                }
                else
                {
                    #region 無異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    LogProcessDesc += ": 無異動紀錄";

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, 0, "[Part] " + LogProcessDesc);
                     
                    //更新 EIS_JOBCONTROL, 並記錄下次轉檔的比對時間 
                    int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, Condition[0].Trim(), EISOldLastUpdate);
                    
                    dboEIS.Commit();

                    #endregion 
                }
               
                #endregion
 
                Console.WriteLine("End Parser : " + LogProcessDesc);

                // 記錄Log : 結束解析ERP來源料號 
                TraceNormalLog("End Parser : " + LogProcessDesc);
                 
                TraceNormalLog("End Sync !");
                TraceNormalLog("-------------------------------------------------------");
                 
                if (AlarmJobFlag)
                {
                    // 寫入Alarm發送EMail
                    string AlarmMsg = WriteAlarmJob(dboMES);
                    if (!AlarmMsg.Equals("Success"))
                    {
                        dboEIS.BeginTransaction();
                        //新增Log (EIS_JOBHIST)
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Alarm] 有錯誤發生, ex:" + AlarmMsg);
                        dboEIS.Commit();
                    }
                }
            }
            catch (Exception e)
            {               
                TraceErrorLog(e);

                dboEIS.BeginTransaction();
                //更新 EIS_JOBCONTROL, 切換State回Idle  
                int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                dboEIS.Commit();
            }
            finally
            {
                if (dboEIS != null)
                {
                    dboEIS.ColseConnect();
                }
                if (dboERP != null)
                {
                    dboERP.ColseConnect();
                }
                if (dboMES != null)
                {
                    dboMES.ColseConnect();
                }                 
            }
        }
         
    }
}