﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OracleClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;

namespace Genesis.Gtimes.EIS
{
    public class EISToMLot : EISBase
    {
        #region 轉換欄位資訊

        /// <summary>
        /// 取得ERP來源資料
        /// </summary>
        /// <returns></returns>
        public DataView GetERPData(string[] Condition)
        {
            string ERPSQL = string.Empty;

            //if (Condition == null)
            //{
                // 第一次同步 
                ERPSQL = @" 
                Select   MTR_LOT,QUANTITY,UNIT,PARTNO 
                From MES_MLOT where STATE = 0  
                ";
//            }
//            else
//            {
//                // 有上次查詢條件
//                string LastModifyDate = Condition[0].Trim();

//                ERPSQL = string.Format(@"
//                SELECT * FROM (
//                    Select   MTR_LOT,QUANTITY,UNIT,PARTNO
//                            , Modified_Time as MODIFY_TIME      
//                    From MES_MLOT where STATE = 0 
//                ) a 
//                WHERE MODIFY_TIME > '{0}' 
//                ORDER BY MODIFY_TIME
//                ", LastModifyDate);
//            }

            DataView dvERP = dboERP.GetData(ERPSQL);
            return dvERP;
        }

        /// <summary>
        /// 新增料號基本資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int WriteMtrLotData(DataBaseObject dbo, DataRowView drvERP)
        {
            // 新增資料表名稱
            string tableName = "MT_LOT";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);

            // 解析對應欄位 
            string Mtr_lot_SID = Guid.NewGuid().ToString();
            string Mtr_lot = drvERP["MTR_LOT"].ToString().Trim();
            string PartNo = drvERP["PARTNO"].ToString().Trim();
            decimal QUANTITY = Commfunction.ConvertToDecimal(drvERP["QUANTITY"].ToString().Trim());
            string Unit = drvERP["UNIT"].ToString().Trim();
            string ProductSID = "GTI1605172105191056005325";

            icb.InsertColumn("MTR_LOT_SID", Mtr_lot_SID);
            icb.InsertColumn("MTR_LOT", Mtr_lot);
            icb.InsertColumn("STATUS", "Release");
            icb.InsertColumn("QUANTITY", QUANTITY);
            icb.InsertColumn("UNIT", Unit);

            #region 取 Part & Product
            MESFunction mes = new MESFunction(dbo);
            DataView dvPartNo = mes.GetPartNoData(PartNo);
            if (dvPartNo != null && dvPartNo.Count > 0)
            {
                ProductSID = dvPartNo[0]["PRODUCT_SID"].ToString().Trim();

                icb.InsertColumn("PARTNO_SID", dvPartNo[0]["PARTNO_SID"].ToString().Trim());
                icb.InsertColumn("PARTNO", dvPartNo[0]["PARTNO"].ToString().Trim());
                icb.InsertColumn("PARTNO_VER_SID", dvPartNo[0]["PARTNO_VER_SID"].ToString().Trim());
                icb.InsertColumn("PARTNO_VERSION", dvPartNo[0]["VERSION"].ToString().Trim());
            }
            //系統預設產品
            DataView dvProduct = mes.GetProductData(ProductSID);
            if (dvProduct != null && dvProduct.Count > 0)
            {
                icb.InsertColumn("PRODUCT_SID", ProductSID);
                icb.InsertColumn("PRODUCT", dvProduct[0]["PRODUCT"].ToString().Trim());
            }
            #endregion

            //其他欄位預設值      

            icb.InsertColumn("PRIORITY", "Normal");

            icb.InsertColumn("ROOT_MTR_LOT_SID", Mtr_lot_SID);

            DateTime dt = DateTime.Now;

            icb.InsertColumn("CREATE_USER", WriteUserID);
            icb.InsertColumn("CREATE_DATE", dt);
            icb.InsertColumn("UPDATE_USER", WriteUserID);
            icb.InsertColumn("UPDATE_DATE", dt);

            int iMaster = dbo.ExcuteCommand(icb.GetSqlCommand());

            if (iMaster > 0)
            {
                iMaster += WriteMtrLotCreateData(dbo, Mtr_lot_SID, Mtr_lot, PartNo, QUANTITY, Unit, ProductSID, dt);
                iMaster += WriteMtrLotHistData(dbo, "Write", Mtr_lot_SID, Mtr_lot, QUANTITY, Unit, dt);
            }

            return iMaster;
        }

        /// <summary>
        /// 新增物料批號主檔資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="PartNoSID"></param>
        /// <param name="PartNo"></param>
        /// <param name="PartName"></param>
        /// <param name="dtUpdate"></param>
        /// <returns></returns>
        public int WriteMtrLotCreateData(DataBaseObject dbo, string Mtr_lot_SID, string Mtr_lot, string PartNo, decimal QUANTITY, string Unit, string ProductSID, DateTime dtUpdate)
        {
            // 新增資料表名稱
            string tableName = "MT_LOT_CREATE";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);

            // 解析對應欄位 
            icb.InsertColumn("MTR_LOT_SID", Mtr_lot_SID);
            icb.InsertColumn("MTR_LOT", Mtr_lot);
            icb.InsertColumn("STATUS", "Release");
            icb.InsertColumn("QUANTITY", QUANTITY);
            icb.InsertColumn("UNIT", Unit);

            #region 取 PartType & Product
            MESFunction mes = new MESFunction(dbo);
            DataView dvPartNo = mes.GetPartNoData(PartNo);
            if (dvPartNo != null && dvPartNo.Count > 0)
            {
                ProductSID = dvPartNo[0]["PRODUCT_SID"].ToString().Trim();

                icb.InsertColumn("PARTNO_SID", dvPartNo[0]["PARTNO_SID"].ToString().Trim());
                icb.InsertColumn("PARTNO", dvPartNo[0]["PARTNO"].ToString().Trim());
                icb.InsertColumn("PARTNO_VER_SID", dvPartNo[0]["PARTNO_VER_SID"].ToString().Trim());
                icb.InsertColumn("PARTNO_VERSION", dvPartNo[0]["VERSION"].ToString().Trim());
            }
            //系統預設產品
            DataView dvProduct = mes.GetProductData(ProductSID);
            if (dvProduct != null && dvProduct.Count > 0)
            {
                icb.InsertColumn("PRODUCT_SID", ProductSID);
                icb.InsertColumn("PRODUCT", dvProduct[0]["PRODUCT"].ToString().Trim());
            }
            #endregion

            //其他欄位預設值      

            icb.InsertColumn("PRIORITY", "Normal");

            icb.InsertColumn("CREATE_USER", WriteUserID);
            icb.InsertColumn("CREATE_DATE", dtUpdate);
            return dbo.ExcuteCommand(icb.GetSqlCommand());
        }

        /// <summary>
        /// 新增物料批號歷史資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="PartNoSID"></param>
        /// <param name="PartNo"></param>
        /// <param name="PartName"></param>
        /// <param name="dtUpdate"></param>
        /// <returns></returns>
        public int WriteMtrLotHistData(DataBaseObject dbo, string type, string Mtr_lot_SID, string Mtr_lot, decimal QUANTITY, string Unit, DateTime dtUpdate)
        {
            // 新增資料表名稱
            string tableName = "MT_LOT_HIST";

            InsertCommandBuilder icb = new InsertCommandBuilder(tableName, dbo.databaseType);

            // 解析對應欄位 
            icb.InsertColumn("MTR_LOT_HIST_SID", Guid.NewGuid().ToString());
            icb.InsertColumn("MTR_LOT_SID", Mtr_lot_SID);
            icb.InsertColumn("MTR_LOT", Mtr_lot);
            if (type == "Write")
            {
                icb.InsertColumn("ACTION", "CREATE_START_MTR_LOT");
                icb.InsertColumn("APPLICATION_NAME", "ERPToMLot");
                icb.InsertColumn("ACTION_LINK_SID", Guid.NewGuid().ToString());
                icb.InsertColumn("ACTION_LINK_TABLE", "MT_LOT");
                icb.InsertColumn("NEW_STATUS", "Release");
                icb.InsertColumn("NEW_QUANTITY", QUANTITY);
                icb.InsertColumn("NEW_UNIT", Unit);
            }
            else
            {
                icb.InsertColumn("ACTION", "UPDATE_START_MTR_LOT");
                icb.InsertColumn("APPLICATION_NAME", "ERPToMLot");
                icb.InsertColumn("ACTION_LINK_SID", Guid.NewGuid().ToString());
                icb.InsertColumn("ACTION_LINK_TABLE", "MT_LOT");
                MESFunction mes = new MESFunction(dbo);

                DataView dvMtrLot = mes.GetMLotData(Mtr_lot);
                if (dvMtrLot != null && dvMtrLot.Count > 0)
                {
                    icb.InsertColumn("OLD_STATUS", dvMtrLot[0]["STATUS"].ToString().Trim());
                    icb.InsertColumn("OLD_QUANTITY", dvMtrLot[0]["QUANTITY"].ToString().Trim());
                    icb.InsertColumn("OLD_UNIT", dvMtrLot[0]["UNIT"].ToString().Trim());
                }
                icb.InsertColumn("NEW_STATUS", "Release");
                icb.InsertColumn("NEW_QUANTITY", QUANTITY);
                icb.InsertColumn("NEW_UNIT", Unit);
            }

            icb.InsertColumn("CREATE_USER", WriteUserID);
            icb.InsertColumn("CREATE_DATE", dtUpdate);
            return dbo.ExcuteCommand(icb.GetSqlCommand());
        }


        /// <summary>
        /// 更新料號資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int UpdateMtrLotData(DataBaseObject dbo, DataRowView drvERP, string mtr_lot)
        {
            // 異動資料表名稱
            string tableName = "MT_LOT";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("MTR_LOT", mtr_lot);

            // 解析對應欄位 
            string Mtr_lot_SID = Guid.NewGuid().ToString();
            string PartNo = drvERP["PARTNO"].ToString().Trim();
            decimal QUANTITY = Commfunction.ConvertToDecimal(drvERP["QUANTITY"].ToString().Trim());
            string Unit = drvERP["UNIT"].ToString().Trim();
            string ProductSID = "GTI1605172105191056005325";

            ucb.UpdateColumn("STATUS", "Release");
            ucb.UpdateColumn("QUANTITY", QUANTITY);
            ucb.UpdateColumn("UNIT", Unit);


            #region 取 Part & Product
            MESFunction mes = new MESFunction(dbo);
            DataView dvPartNo = mes.GetPartNoData(PartNo);
            if (dvPartNo != null && dvPartNo.Count > 0)
            {
                ProductSID = dvPartNo[0]["PRODUCT_SID"].ToString().Trim();

                ucb.UpdateColumn("PARTNO_SID", dvPartNo[0]["PARTNO_SID"].ToString().Trim());
                ucb.UpdateColumn("PARTNO", dvPartNo[0]["PARTNO"].ToString().Trim());
                ucb.UpdateColumn("PARTNO_VER_SID", dvPartNo[0]["PARTNO_VER_SID"].ToString().Trim());
                ucb.UpdateColumn("PARTNO_VERSION", dvPartNo[0]["VERSION"].ToString().Trim());
            }
            //系統預設產品
            DataView dvProduct = mes.GetProductData(ProductSID);
            if (dvProduct != null && dvProduct.Count > 0)
            {
                ucb.UpdateColumn("PRODUCT_SID", ProductSID);
                ucb.UpdateColumn("PRODUCT", dvProduct[0]["PRODUCT"].ToString().Trim());
            }
            #endregion


            DateTime dt = DateTime.Now;

            //其他欄位預設值
            ucb.UpdateColumn("UPDATE_USER", WriteUserID);
            ucb.UpdateColumn("UPDATE_DATE", dt);

            int iMaster = dbo.ExcuteCommand(ucb.GetSqlCommand());

            if (iMaster > 0)
            {
                iMaster += UpdateMtrLotCreateData(dbo, mtr_lot, PartNo, QUANTITY, Unit, ProductSID, dt);
                iMaster += WriteMtrLotHistData(dbo, "Update", Mtr_lot_SID, mtr_lot, QUANTITY, Unit, dt);

            }

            return iMaster;
        }

        /// <summary>
        /// 更新料號版本資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="PartNo"></param>
        /// <param name="PartName"></param>
        /// <param name="dtUpdate"></param>
        /// <returns></returns>
        public int UpdateMtrLotCreateData(DataBaseObject dbo, string mtr_lot, string PartNo, decimal QUANTITY, string Unit, string ProductSID, DateTime dtUpdate)
        {
            // 異動資料表名稱
            string tableName = "MT_LOT_CREATE";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("MTR_LOT", mtr_lot);

            ucb.UpdateColumn("STATUS", "Release");
            ucb.UpdateColumn("QUANTITY", QUANTITY);
            ucb.UpdateColumn("UNIT", Unit);

            #region 取 PartType & Product
            MESFunction mes = new MESFunction(dbo);
            DataView dvPartNo = mes.GetPartNoData(PartNo);
            if (dvPartNo != null && dvPartNo.Count > 0)
            {
                ProductSID = dvPartNo[0]["PRODUCT_SID"].ToString().Trim();

                ucb.UpdateColumn("PARTNO_SID", dvPartNo[0]["PARTNO_SID"].ToString().Trim());
                ucb.UpdateColumn("PARTNO", dvPartNo[0]["PARTNO"].ToString().Trim());
                ucb.UpdateColumn("PARTNO_VER_SID", dvPartNo[0]["PARTNO_VER_SID"].ToString().Trim());
                ucb.UpdateColumn("PARTNO_VERSION", dvPartNo[0]["VERSION"].ToString().Trim());
            }
            //系統預設產品
            DataView dvProduct = mes.GetProductData(ProductSID);
            if (dvProduct != null && dvProduct.Count > 0)
            {
                ucb.UpdateColumn("PRODUCT_SID", ProductSID);
                ucb.UpdateColumn("PRODUCT", dvProduct[0]["PRODUCT"].ToString().Trim());
            }
            #endregion

            return dbo.ExcuteCommand(ucb.GetSqlCommand());
        }

        /// <summary>
        /// 更新ERP處理狀態
        /// </summary>       
        public void UpdateERPState(DataBaseObject dbo, string mtr_lot, string State)
        {
            // 異動資料表名稱
            string tableName = "MES_MLOT";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);

            // 建立異動條件
            ucb.WhereAnd("MTR_LOT", mtr_lot);

            ucb.UpdateColumn("STATE", State);

            dbo.ExcuteCommand(ucb.GetSqlCommand());
        }

        #endregion

        private string WriteAlarmJob(DataBaseObject dbo)
        {
            string SendSubject = "[EIS]轉檔錯誤通知--物料批號";
            string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;

            return MESFunction.WriteAlarmJob(dbo, SendSubject, SendFilePath);
        }

        public void Start(string[] args)
        {
            nlog = LogManager.GetLogger(this.GetType().Name);

            DateTime Job_StartTime = DateTime.Now;

            //Keep Process SID, 檢測每次轉檔執行緒
            string EIS_SID = "MLot";
            LogAction LogAction;

            // 記錄是否發送Email
            bool AlarmJobFlag = false;

            DateTime EISOldLastUpdate = DateTime.Now;

            try
            {
                if (args.Length > 0)
                {
                    EIS_SID = args[0].Trim();
                }

                // 連線資料庫
                ConnectDataBase();

                // 記錄Log : 開始轉料號
                TraceNormalLog("Start Sync !");

                #region 判斷是否有multiProcess狀況

                bool multiProcess = false;
                string[] Condition = null;

                //取出EIS_JOB執行狀況,回傳至EISResult  
                dboEIS.BeginTransaction();
                EISResult EIS = EISFunction.CheckEISResult(dboEIS, EIS_SID, Condition, EISOldLastUpdate);
                dboEIS.Commit();

                multiProcess = EIS.multiProcess;
                Condition = EIS.Condition;
                EISOldLastUpdate = EIS.EISOldLastUpdate;

                if (multiProcess)
                {
                    // 當 JOB 狀態為 Run, 表示有平行處理的問題 
                    // 新增紀錄Log (EIS_JOBHIST), 不更新JOB CONTROL 
                    dboEIS.BeginTransaction();
                    int iLogCount =
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[MLot] Job State=Run, Muti Process!!");
                    dboEIS.Commit();

                    TraceNormalLog("JOB 狀態為 Run ,可能有其他相同程式也在執行");

                    return;
                }

                #endregion

                #region Get ERP Data

                //取得ERP來源資料
                DataView dvERP = GetERPData(Condition);

                #endregion

                #region Parser ERP Data

                // 記錄Log : 開始解析ERP來源料號 
                TraceNormalLog("Start Parser : " + dvERP.Count.ToString());

                string NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                string LogProcessDesc = string.Empty;

                if (Condition == null)
                {
                    //第一次同步
                    LogProcessDesc = "First Sync";
                    LogAction = LogAction.Insert;
                }
                else
                {
                    //有上次查詢條件
                    LogProcessDesc = "Data Sync";
                    LogAction = LogAction.DataSync;
                }

                if (dvERP != null && dvERP.Count > 0)
                {
                    #region 有異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    MESFunction mes = new MESFunction(dboMES);

                    //異動成功筆數
                    int iUpdSuccess = 0;
                    int iInsSuccess = 0;

                    //累計異動筆數
                    int iInsertCount = 0;
                    int iUpdateCount = 0;

                    //累計達N筆,執行Commit
                    int iCommitCount = 0;

                    //檢查KEY
                    string sMLot = string.Empty;

                    dboMES.BeginTransaction();

                    for (int i = 0; i < dvERP.Count; i++)
                    {
                        iUpdSuccess = 0;
                        iInsSuccess = 0;
                        sMLot = dvERP[i]["MTR_LOT"].ToString().Trim();

                        //檢查是否已有Mtr_lot
                        if (mes.IsExistMLot(sMLot))
                        {
                            try
                            {
                                string sMlotStatus = mes.GetMLOTStatus(sMLot);

                                if (sMlotStatus != "Create" && sMlotStatus != "Release")
                                {
                                    UpdateERPState(dboERP, sMLot, "2");
                                    // 記錄Log 
                                    TraceNormalLog("UPDATE|" + sMLot + " ex:MES物料批號狀態非Create，Release，不允許變更。");
                                }
                                else
                                {
                                    //回傳UPDATE成功筆數 
                                    iUpdSuccess = UpdateMtrLotData(dboMES, dvERP[i], sMLot);

                                    if (iUpdSuccess > 0)
                                    {
                                        iUpdateCount += 1;
                                        iCommitCount += 1;
                                        //Console.WriteLine(" U " + iUpdateCount.ToString() + ".PartNo :" + sPartNo);
                                        UpdateERPState(dboERP, sMLot, "1");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                UpdateERPState(dboERP, sMLot, "2");

                                // 記錄Log
                                TraceNormalLog("UPDATE|" + sMLot + " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }
                        else
                        {
                            try
                            {
                                //回傳INSERT成功筆數 
                                iInsSuccess = WriteMtrLotData(dboMES, dvERP[i]);
                                if (iInsSuccess > 0)
                                {
                                    iInsertCount += 1;
                                    iCommitCount += 1;
                                    //Console.WriteLine(" I " + iInsertCount.ToString() + ".PartNo :" + sPartNo);
                                    UpdateERPState(dboERP, sMLot, "1");
                                }
                            }
                            catch (Exception ex)
                            {
                                UpdateERPState(dboERP, sMLot, "2");

                                // 記錄Log 
                                TraceNormalLog("INSERT|" + sMLot +  " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }

                        //不管新增或異動,每3000就Commit
                        if (iCommitCount >= 3000)
                        {
                            try
                            {
                                dboMES.Commit();
                                TraceNormalLog("寫入異動資料" + iCommitCount.ToString() + "筆!");

                                dboMES.BeginTransaction();
                                iCommitCount = 0;  //清除重來

                                //NextUpdateDate = dvERP[i]["MODIFY_TIME"].ToString();
                                //if (NextUpdateDate.Equals(""))
                                    NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);

                                // *** 重要 *** 
                                // 記錄下次轉檔的比對時間(NextUpdateDate),State不變
                                dboEIS.BeginTransaction();
                                int iEIS_1 = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Run, NextUpdateDate, EISOldLastUpdate);
                                dboEIS.Commit();
                            }
                            catch (Exception ex)
                            {
                                TraceErrorLog(ex);
                                dboMES.Rollback();
                            }
                        }
                    }

                    //餘數未Commit
                    if (iCommitCount < 3000)
                    {
                        try
                        {
                            dboMES.Commit();
                            TraceNormalLog("寫入異動資料餘數" + iCommitCount.ToString() + "筆!");
                            iCommitCount = 0;
                        }
                        catch (Exception ex)
                        {
                            TraceErrorLog(ex);
                            dboMES.Rollback();
                        }
                    }

                    if (AlarmJobFlag == false)
                    {
                        int iRowCount = iUpdateCount + iInsertCount;
                        LogProcessDesc += ": Check Row=" + dvERP.Count + ", Update Row=" + iUpdateCount + ", Insert Row=" + iInsertCount;

                        //NextUpdateDate = dvERP[dvERP.Count - 1]["MODIFY_TIME"].ToString();
                        //if (NextUpdateDate.Equals(""))
                            NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);

                        dboEIS.BeginTransaction();

                        //新增Log (EIS_JOBHIST)
                        int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, iRowCount, "[MLot] " + LogProcessDesc);

                        // *** 重要 *** 
                        // 更新 EIS_JOBCONTROL,記錄下次轉檔的比對時間(NextUpdateDate) 
                        int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, NextUpdateDate, EISOldLastUpdate);

                        dboEIS.Commit();
                    }
                    else
                    {
                        dboEIS.BeginTransaction();
                        //更新 EIS_JOBCONTROL, 切換State回Idle  
                        int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                        dboEIS.Commit();
                    }
                    #endregion
                }
                else
                {
                    #region 無異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    LogProcessDesc += ": 無異動紀錄";

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, 0, "[MLot] " + LogProcessDesc);

                    //更新 EIS_JOBCONTROL, 並記錄下次轉檔的比對時間 
                    int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, Condition[0].Trim(), EISOldLastUpdate);

                    dboEIS.Commit();

                    #endregion
                }

                #endregion

                Console.WriteLine("End Parser : " + LogProcessDesc);

                // 記錄Log : 結束解析ERP來源料號 
                TraceNormalLog("End Parser : " + LogProcessDesc);

                TraceNormalLog("End Sync !");
                TraceNormalLog("-------------------------------------------------------");

                if (AlarmJobFlag)
                {
                    // 寫入Alarm發送EMail
                    string AlarmMsg = WriteAlarmJob(dboMES);
                    if (!AlarmMsg.Equals("Success"))
                    {
                        dboEIS.BeginTransaction();
                        //新增Log (EIS_JOBHIST)
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Alarm] 有錯誤發生, ex:" + AlarmMsg);
                        dboEIS.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                TraceErrorLog(e);

                dboEIS.BeginTransaction();
                //更新 EIS_JOBCONTROL, 切換State回Idle  
                int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                dboEIS.Commit();
            }
            finally
            {
                if (dboEIS != null)
                {
                    dboEIS.ColseConnect();
                }
                if (dboERP != null)
                {
                    dboERP.ColseConnect();
                }
                if (dboMES != null)
                {
                    dboMES.ColseConnect();
                }
            }
        }

    }
}
