﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.OracleClient;
using EIS.CommonLibrary;
using NLog;
using System.IO;

namespace Genesis.Gtimes.EIS
{
    public class EISToRoute : EISBase
    {        
        #region 轉換欄位資訊

        /// <summary>
        /// 取得ERP來源資料
        /// </summary>
        /// <returns></returns>
        public DataView GetERPData(string[] Condition)
        {
            string ERPSQL = @"SELECT * FROM PF_ROUTE_VER where CONVERT(varchar(100), EXPIRYDATE, 111)<getdate() and VERSION_STATE='Enable'";

            DataView dvERP = dboERP.GetData(ERPSQL);
            return dvERP;
        }
  
        /// <summary>
        /// 更新供應商資訊
        /// </summary>
        /// <param name="dbo"></param>
        /// <param name="drvERP"></param>
        /// <returns></returns>
        public int UpdateRouteVerData(DataBaseObject dbo, DataRowView drvERP, string RouteVerSid)
        {
            // 異動資料表名稱
            string tableName = "PF_ROUTE_VER";

            UpdateCommandBuilder ucb = new UpdateCommandBuilder(tableName, dbo.databaseType);
 
            // 建立異動條件
            ucb.WhereAnd("ROUTE_VER_SID", RouteVerSid);
           
            ucb.UpdateColumn("VERSION_STATE", "Disable");
                                        
            DateTime dt = DateTime.Now;

            //其他欄位預設值
            ucb.UpdateColumn("UPDATE_USER", WriteUserID);
            ucb.UpdateColumn("UPDATE_DATE", dt);
             
            int iMaster = dbo.ExcuteCommand(ucb.GetSqlCommand());
 
            return iMaster;
        }

        #endregion 
          
        private string WriteAlarmJob(DataBaseObject dbo)
        {
            string SendSubject = "[EIS]轉檔錯誤通知--供應商";
            string LogFileName = DateTime.Today.ToString("yyyy-MM-dd-") + this.GetType().Name + ".txt";
            string SendFilePath = Directory.GetCurrentDirectory() + "\\log\\" + this.GetType().Name + "\\" + LogFileName;

            return MESFunction.WriteAlarmJob(dbo, SendSubject, SendFilePath);
        }

        public void Start(string[] args)
        {
            //依此TXT中可分辨轉檔JOB名稱
            nlog = LogManager.GetLogger(this.GetType().Name);

            DateTime Job_StartTime = DateTime.Now;

            //Keep Process SID, 檢測每次轉檔執行緒
            string EIS_SID = "Route";
            LogAction LogAction;

            // 記錄是否發送Email
            bool AlarmJobFlag = false;

            DateTime EISOldLastUpdate = DateTime.Now;
    
            try
            {
                if (args.Length > 0)
                {
                    EIS_SID = args[0].Trim();
                }

                // 連線資料庫
                ConnectDataBase();
               
                // 記錄Log : 開始轉供應商
                TraceNormalLog("Start Sync !");

                #region 判斷是否有multiProcess狀況

                bool multiProcess = false;
                string[] Condition = null;

                //取出EIS_JOB執行狀況,回傳至EISResult  
                dboEIS.BeginTransaction();
                EISResult EIS = EISFunction.CheckEISResult(dboEIS, EIS_SID, Condition, EISOldLastUpdate);
                dboEIS.Commit();

                multiProcess = EIS.multiProcess;
                Condition = EIS.Condition;
                EISOldLastUpdate = EIS.EISOldLastUpdate;

                if (multiProcess)
                {
                    // 當 JOB 狀態為 Run, 表示有平行處理的問題 
                    // 新增紀錄Log (EIS_JOBHIST), 不更新JOB CONTROL 
                    dboEIS.BeginTransaction();
                    int iLogCount = 
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Route] Job State=Run, Muti Process!!");
                    dboEIS.Commit();

                    TraceNormalLog("JOB 狀態為 Run ,可能有其他相同程式也在執行");
                     
                    return;
                }

                #endregion

                #region Get ERP Data

                //取得ERP來源資料
                DataView dvERP = GetERPData(Condition);
                 
                #endregion

                #region Parser ERP Data

                // 記錄Log : 開始解析ERP來源供應商 
                TraceNormalLog("Start Parser : " + dvERP.Count.ToString());

                string NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                string LogProcessDesc = string.Empty;
                
                if (Condition == null)
                {
                    //第一次同步
                    LogProcessDesc =  "First Sync";
                    LogAction = LogAction.Insert;
                }
                else
                {
                    //有上次查詢條件
                    LogProcessDesc = "Data Sync";
                    LogAction = LogAction.DataSync;
                }

                int iRowCount = 0;
                int iInsertCount = 0;
                int iUpdateCount = 0;
              
                if (dvERP != null && dvERP.Count > 0)   
                {
                    #region 有異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)
                    
                    MESFunction mes = new MESFunction(dboMES);
                    int iUpdSuccess = 0;
                    string sRouteVerSid = string.Empty;

                    dboMES.BeginTransaction();

                    for (int i = 0; i < dvERP.Count; i++)
                    {
                        iUpdSuccess = 0;
                        sRouteVerSid = dvERP[i]["ROUTE_VER_SID"].ToString().Trim();
                        
                        //檢查Routes是否超过有效时间
                        if (mes.IsExistRoute(sRouteVerSid))
                        {
                            try
                            {
                                //回傳UPDATE成功筆數                               
                                iUpdSuccess = UpdateRouteVerData(dboMES, dvERP[i], sRouteVerSid);
                                if (iUpdSuccess > 0)
                                {
                                    iUpdateCount += 1; 
                                }                                                              
                             }
                            catch (Exception ex)
                            {
                                // 記錄Log
                                TraceNormalLog("UPDATE|" + sRouteVerSid + "|" + dvERP[i]["ROUTE"].ToString() + " ex:" + GetExceptionAllDetails(ex));
                                AlarmJobFlag = true;
                            }
                        }
                    }

                    iRowCount = iUpdateCount + iInsertCount;
                    LogProcessDesc += ": Check Row=" + dvERP.Count + ", Update Row=" + iUpdateCount + ", Insert Row=" + iInsertCount;
                    //NextUpdateDate = dvERP[dvERP.Count - 1]["MODIFY_TIME"].ToString();
                    //if (NextUpdateDate.Equals(""))
                        NextUpdateDate = DateTime.Now.ToString(Full_DateTime_Format);
                   
                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, iRowCount, "[Route] " + LogProcessDesc);
                    int EIS_JobControlCount = 0;

                    if (AlarmJobFlag == false)
                    {
                        // *** 重要 *** 
                        // 更新 EIS_JOBCONTROL,記錄下次轉檔的比對時間(NextUpdateDate) 
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, NextUpdateDate, EISOldLastUpdate);
                    }
                    else
                    {
                        //更新 EIS_JOBCONTROL, 切換State回Idle  
                        EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                    }

                    if (EIS_LogCount > 0 && EIS_JobControlCount > 0) 
                    {
                        try
                        {
                            dboEIS.Commit();
                            dboMES.Commit();
                        }
                        catch (Exception ex)
                        {
                            TraceErrorLog(ex);
                            dboMES.Rollback();
                        }
                    }
                    else
                    {
                        dboEIS.Commit();
                        dboMES.Rollback();
                    }

                    #endregion
                }
                else
                {
                    #region 無異動紀錄 (記錄 EIS_JOBHIST, 更新 EIS_JOBCONTROL 之 JOB_STATE=IDLE)

                    LogProcessDesc += ": 無異動紀錄";

                    dboEIS.BeginTransaction();

                    //新增Log (EIS_JOBHIST)
                    int EIS_LogCount = EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Normal, LogAction, 0, "[Route] " + LogProcessDesc);
                     
                    //更新 EIS_JOBCONTROL, 並記錄下次轉檔的比對時間 
                    int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, Condition[0].Trim(), EISOldLastUpdate);
                    
                    dboEIS.Commit();

                    #endregion      
                }
               
                #endregion

                Console.WriteLine("End Parser : " + LogProcessDesc);

                // 記錄Log : 結束解析ERP來源供應商 
                TraceNormalLog("End Parser : " + LogProcessDesc); 
                TraceNormalLog("End Sync !");
                TraceNormalLog("-------------------------------------------------------");

                if (AlarmJobFlag)
                {
                    // 寫入Alarm發送EMail
                    string AlarmMsg = WriteAlarmJob(dboMES);
                    if (!AlarmMsg.Equals("Success"))
                    {
                        dboEIS.BeginTransaction();
                        //新增Log (EIS_JOBHIST)
                        EISFunction.CreateLog(dboEIS, Job_StartTime, LogState.Error, LogAction.SkipJob, 0, "[Alarm] 有錯誤發生, ex:" + AlarmMsg);
                        dboEIS.Commit();
                    }
                }
            }
            catch (Exception e)
            {               
                TraceErrorLog(e);

                dboEIS.BeginTransaction();
                //更新 EIS_JOBCONTROL, 切換State回Idle  
                int EIS_JobControlCount = EISFunction.UpdateJobControl(dboEIS, EIS_SID, JobState.Idle, EISOldLastUpdate);
                dboEIS.Commit();
            }
            finally
            {
                if (dboEIS != null)
                {
                    dboEIS.ColseConnect();
                }
                if (dboERP != null)
                {
                    dboERP.ColseConnect();
                }
                if (dboMES != null)
                {
                    dboMES.ColseConnect();
                }
            }
        }
         
    }
}